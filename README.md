Multicore Parallel Simplex
========
This project contains an analysis of the speedup and efficiency of our proposal of a multicore parallel implementation of the standard simplex algorithm using OpenMP. The tests are done with random linear programming problems. Anyone with any multicore computer can test and have their own results of performance.

# Required Packages
	+ GCC >= 4.9.0
	+ Octave >= 4.2

# Organization
  
	Most importants foldes and files.

		+ cplex
       		- Makefile -> compile IBM CPLEX file.
       		- scplex.cpp -> a IBM CPLEX file.
     	+ cpp
       		- Makefile -> compile mainP.cpp and mainS.cpp.
       		- mainS.cpp -> sequencial simplex implementation file.
       		- mainP.cpp -> parallel multicore simplex implementation file.
       	+ data -> folder with the generated problems files.
       	+ matlab_octave
       		+ graph -> the generated graphs are here.
       		- analysis_octave -> octave script to generate the graphs.
       		- analysis_matlab -> matlab script to generate our results graphs.
       		- dataoctave -> octave script to generate the random problems.
       	+ results -> folder with the results of the tests.
# Configuration 
  
  Change the folowing variables inside main.sh:
  
  	- PATH_PROJECT -> for your computer.
  	- L1 -> The initial amount of problem constraints
    - L2 -> The final amount of problem constraints
	- C1 -> The initial amount of problem variables
    - C2 -> The final amount of problem variables
	- K  -> The amount of problems to the same size.
    - R  -> The amount of execution to the same problem.
# Usage
  
  ./main.sh

### Obsevations
  If you want to see our results in a 64 cores server, execute the analysis_matlab file inside *matlab_octave* folder.