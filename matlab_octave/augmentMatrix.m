function [A_new, b_new, c_new] = augmentMatrix(A_old, b_old, c_old, add_m, add_n, d)
    % Ensure b_old and c_old are column vectors
    if isrow(b_old)
        b_old = b_old'; % Transpose if it's a row vector
    end
    if isrow(c_old)
        c_old = c_old'; % Transpose if it's a row vector
    end

    % Add rows
    A_new_rows = sprand(add_m, size(A_old, 2), d) * 50;
    A_new = [A_old; A_new_rows];
    b_new = [b_old; sprand(add_m, 1, d) * 50]; % Adjust range as needed

    % Add columns
    A_new_cols = sprand(size(A_new, 1), add_n, d) * 50;
    A_new = [A_new A_new_cols];
    c_new = [c_old; zeros(add_n, 1)]; % Extend c with zeros for new variables

    % Ensure c_new is non-zero
    if all(c_new == 0)
        c_new(randi(numel(c_new))) = rand; % Ensure at least one non-zero element
    end
end
