function save_problem(m, n, d, problem)
    path_data = '/home/vrrcelestino/Paper_simplex/MulticoreParallelSimplex/data';
    full_file_path = sprintf('%s/%dx%d_%d', path_data, m, n, d);
    
    % Open and write to the file
    fid = fopen(full_file_path, 'w');
    for i = 1:size(problem,1)
        fprintf(fid, '%f ', problem(i,:));
        fprintf(fid, '\n');
    end
    fclose(fid);
end