function createspeedup3d(zdata1,thread)
% Create figure
figure1 = figure('units','normalized','outerposition',[0 0 1 1]);

% Create axes
axes1 = axes('Parent',figure1,'ZTick',[0:2:30],...
    'ZMinorTick','on',...
    'ZMinorGrid','on',...
    'YTickLabel',{'256','512','1024','2048', '4096','8192'},...
    'YTick',[256,512,1024,2048, 4096,8192],...
    'XTickLabel',{'256','512','1024','2048', '4096','8192'},...
    'XTick',[256,512,1024,2048, 4096,8192],...
    'Position',[0.0926204819277108 0.109677419354839 0.836596385542169 0.77741935483871],...
    'FontSize',14);
%% Uncomment the following line to preserve the X-limits of the axes
 xlim(axes1,[256 8192]);
%% Uncomment the following line to preserve the Y-limits of the axes
 ylim(axes1,[256 8192]);
%% Uncomment the following line to preserve the Z-limits of the axes
 zlim(axes1,[0 30]);
view(axes1,[347.3 54.4000000000001]);
grid(axes1,'on');
hold(axes1,'all');

% Create surf
surf([256 512 1024 2048 4096 8192],[256 512 1024 2048 4096 8192],zdata1,'Parent',axes1);

% Create xlabel
xlabel('Constraint','FontSize',14);

% Create ylabel
ylabel('Variables','FontSize',14);

% Create zlabel
zlabel('Speedup','FontSize',14);

% Create title
title(sprintf('Speedup with %d threads',thread),'FontSize',18);

saveas(gcf,sprintf('speedup3d_%d_threads_median_simplex.png',thread));
