#! /opt/npad/shared/spack-v.0.21.0/opt/spack/linux-rocky8-haswell/gcc-8.5.0/octave-8.2.0-meiw63frywe75codials5viumjpdejw7/bin/octave -qf

arg_list = argv ();

l1=str2num(arg_list{1});
l2=str2num(arg_list{2});
NumbOfCol=str2num(arg_list{3});
k=str2num(arg_list{4});
r=str2num(arg_list{5});
numbOfThreads=log2(str2num(arg_list{6}));
PATH_PROJECT=arg_list{7};

addpath (strcat(PATH_PROJECT,'/matlab_octave/'));

path_data = strcat(PATH_PROJECT,'/data/')';

out_cpp_parall = importdata(strcat(PATH_PROJECT,'/results/results_octave.csv'));

timeParallel = out_cpp_parall(:,5);
iteracoesParallel = out_cpp_parall(:,6);

% l1=2;
% l2=6;
% NumbOfCol=5;
% k=3;
% r=5;
% numbOfThreads=2;

NumbOfLin = abs(l2-l1)+1;
fprintf('l1 %d l2 %d numbOfCol %d k %d r %d numbOfThreads %d NumbOfLin  %d\n',l1,l2,NumbOfCol,k,r,numbOfThreads,NumbOfLin);

%fixa a quantidade de linha e varia a quantidade de colunas
timeParallel = reshape(timeParallel,numbOfThreads+1,length(timeParallel)/(numbOfThreads+1))';

timeParallelM = timeParallel; % k=1 and r=1

%timeParallelM =[];

%for i=1:r:size(timeParallel,1)
%    aux = [];
%  for t=1:numbOfThreads
%    aux = [aux median(timeParallel(i:(i+r-1),t))];
%  end
%  timeParallelM = [timeParallelM; aux];
%end

%timeRevista = [];

%for i=1:k:size(timeParallelM,1)
%      timeRevista = [timeRevista; median(timeParallelM(i:(i+k-1),:))];
%end

speedup = [];
    
%for i=1:size(timeParallelM,2)
%      speedup = [speedup (timeParallelM(:,1)*2)./timeParallelM(:,i)];
%end

for i=2:size(timeParallelM,2)
      speedup = [speedup (timeParallelM(:,1))./timeParallelM(:,i)];
end

speedupM = speedup;

%speedupM = [];

% k=1
%for i=1:k:size(speedup,1)
%      speedupM = [speedupM; median(speedup(i:(i+k-1),:)) ];
%end


totalOfProblems = NumbOfLin*NumbOfCol;

speedupINV = [];
for i=1:NumbOfLin
    speedupINV = [speedupINV; speedupM(i:NumbOfLin:totalOfProblems,:) ];
end

efficiency = [];
threads = 2.^(1:numbOfThreads);
for i=1:size(speedupM,1)
    efficiency = [efficiency; speedupM(i,:)./threads];
end

efficiencyINV = [];
for i=1:totalOfProblems
    efficiencyINV = [efficiencyINV; efficiency(i:NumbOfLin:totalOfProblems,:) ];
end

cons = 2.^(l1:l2);
s = cell(NumbOfLin,1);
%% Uncomment here to generate speedup graphs with variabels fixed
v=1;

for i=1:NumbOfLin:totalOfProblems
    
    for j=1:NumbOfLin
        s(j) = sprintf('%dx%d',cons(j),cons(v));
    end
    
    createspeedupinvoctave(speedupINV(i:(i+NumbOfLin-1),:)',s,cons(v),threads,PATH_PROJECT);%speedup with variable fixed
    v= v+1;
end

%% Uncomment here to generate speedup graphs with constraint fixed
c=1;

for i=1:NumbOfLin:totalOfProblems
    for j=1:NumbOfLin
        s(j) = sprintf('%dx%d',cons(c),cons(j));
    end
    createspeedupthreadsoctave(speedupM(i:(i+NumbOfLin-1),:)',s,cons(c),threads,PATH_PROJECT);
    c= c+1;
end

%% Uncomment here to generate efficiency 2d graphs
n=1;
for i=1:NumbOfLin:totalOfProblems
    createefficiencyoctave(efficiency(i:(i+NumbOfLin-1),:),cons,n,threads,PATH_PROJECT); %FIX CONSTRAINT
    createefficiencyinvoctave(efficiencyINV(i:(i+NumbOfLin-1),:),cons,n,threads,PATH_PROJECT);
    n= n+1;
end

%% Uncomment here to generate efficiency 3d graphs
 d = reshape(efficiency(:,end),NumbOfLin,NumbOfCol);
 createefficiency3doctave(d,threads(end),cons,PATH_PROJECT);

