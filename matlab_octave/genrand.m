function [ppl, f, A, b] = genrand(m, n, d)
    % Generate a sparse matrix A with density d
    A = sprand(m, n, d) * 50; % Scale to adjust range of values

    % Generate x with non-negative elements
    x = sparse(max(0, 10 * rand(n, 1)));

    % Generate b as a product of A and x
    b = A * x;

    % Ensure b is non-negative by adjusting negative elements
    b(b < 0) = -b(b < 0);

    % Generate a random vector u for cost vector calculation
    u = rand(m, 1) * 2 - 1; % Uniform distribution between -1 and 1

    % Generate cost vector c ensuring it's non-zero
    c = A' * u;
    if all(c == 0)
        c(randi(numel(c))) = rand; % Ensure at least one non-zero element
    end

    % Combine into the simplex tableau format
    f = c'; % Objective function (-c input for maximization problem in mainS.cpp and mainP.cpp)
    ppl = [A eye(m) b; f zeros(1, m + 1)]; % ppl contains the tableau matrix
end
