%#! /usr/bin/octave-cli -qf
#! /opt/npad/shared/spack-v.0.21.0/opt/spack/linux-rocky8-haswell/gcc-8.5.0/octave-8.2.0-meiw63frywe75codials5viumjpdejw7/bin/octave-cli qf
%uso ./data.m m n d k

addpath('/home/vrrcelestino/Paper_simplex/MulticoreParallelSimplex/matlab_octave'); %Change the absolute path to your computer

path_data = '/home/vrrcelestino/Paper_simplex/MulticoreParallelSimplex/data';%Change the absolute path to your computer

arg_list = argv ();

m=str2num(arg_list{1});
n=str2num(arg_list{2});
d=str2num(arg_list{3});
k=str2num(arg_list{4});

printf('m %d n %d d %d k %d\n',m,n,d,k);

[problem,f,A,b] = genrand(m,n,d);

% Define the full path for the file
%%full_file_path = fullfile(path_data, [int2str(m), 'x', int2str(n), '_', int2str(k)]);
full_file_path = sprintf('%s/%dx%d_%d', path_data, m, n, k);

% Open the file with the full path
fid = fopen(full_file_path, 'w');

%fid = fopen(strcat(path_data,int2str(m),'x', int2str(n),'_',int2str(k)), 'w');

printf('Writing in the file');

for i = 1 : size(problem,1)
    if mod(i,1000) == 0
       printf('%d percents \n', (i/size(problem,1))*100);
    end      

    for j = 1 : size(problem,2)
        fprintf(fid, '%f ', problem(i,j));
    end
    
    fprintf(fid,'\n');
end
fclose(fid);

