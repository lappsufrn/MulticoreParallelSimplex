function createspeedupthreadsoctave(YMatrix1,s,numb,threads,path)

% Create figure
figure;

% Create axes
axes1 = axes;
hold(axes1,'on');

% Create multiple lines using matrix input to plot
plot1 = plot(YMatrix1,'LineWidth',1,'Color',[0 0 0],'MarkerSize',8);

markers =['+','o','p','h','d','s','*','.','x','^','v','>','<'];

for i=1:length(s)
    set(plot1(i),'DisplayName',char(s(i)),'Marker',markers(i));
end

% set(plot1(1),'DisplayName',char(s(1)),'Marker','+','MarkerSize',6);
% set(plot1(2),'DisplayName',char(s(2)),'Marker','o');
% set(plot1(3),'DisplayName',char(s(3)),'Marker','pentagram');
% set(plot1(4),'DisplayName',char(s(4)),'Marker','hexagram');
% set(plot1(5),'DisplayName',char(s(5)),'Marker','diamond');
% set(plot1(6),'DisplayName',char(s(6)),'Marker','square');

% Create xlabel
xlabel('Threads'); 

% Create title
% title(sprintf('Speedup with %d constraints fixed',constraints));

c = cell(length(threads),1);
for i=1:length(threads)
    c(i) = int2str(threads(i));
end

% Create ylabel
ylabel('Speedup');
ylim(axes1,[0 36]);
box(axes1,'on');
% Set the remaining axes properties
set(axes1,'XTick',[1:length(threads)],'XTickLabel',c,...
    'YGrid','on','YMinorTick','on','YTick',...
    [0:2:36]);


hold(axes1,'off');
% Create legend
%legend(axes1,'show','Location','northwest');
legend(axes1,'Location','northwest');

saveas(gcf,strcat(path,sprintf('/matlab_octave/graph/speedup/Speedup_%d_constraints_median_simplex.png',numb)));