function createefficiency3doctave(zdata1,thread, cons,path)

% Create figure
figure1 = figure('units','normalized','outerposition',[0 0 1 1]);

c = cell(length(cons),1);
for i=1:length(cons)
    c(i) = int2str(cons(i));
end

% Create axes
axes1 = axes('Parent',figure1,'ZTick',[0 0.2 0.4 0.6 0.8 1 1.2],...
    'ZMinorTick','on',...
    'ZMinorGrid','on',...
    'YTickLabel',c,...
    'YTick',cons,...
    'XTickLabel',c,...
    'XTick',cons,...
    'Position',[0.0926204819277108 0.109677419354839 0.836596385542169 0.77741935483871],...
    'FontSize',14);
%% Uncomment the following line to preserve the X-limits of the axes
 xlim(axes1,[cons(1) cons(end)]);
%% Uncomment the following line to preserve the Y-limits of the axes
 ylim(axes1,[cons(1) cons(end)]);
%% Uncomment the following line to preserve the Z-limits of the axes
 zlim(axes1,[0 1.2]);
view(axes1,[347.3 54.4000000000001]);
grid(axes1,'on');
hold(axes1,'all');

% Create surf
surf(cons,cons,zdata1,'Parent',axes1);

% Create xlabel
xlabel('Constraint','FontSize',14);

% Create ylabel
ylabel('Variables','FontSize',14);

% Create zlabel
zlabel('Efficiency','FontSize',14);

% Create title
title(sprintf('Efficiency with %d threads',thread),'FontSize',18);

% a = 8388608;
% c = [256 384 512 768 1024 1536 2048 3072 4096 6144 8192]
% v = (a-(c.*c))./c
plot3(axes1,X1,Y1,Z1,'Linewidth',3,'Color','r');
 
saveas(gcf,strcat(path,sprintf('/matlab_octave/graph/efficiency/Efficiency3d_%d_threads_median_simplex.png',thread)));
