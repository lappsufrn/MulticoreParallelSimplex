function createefficiencyoctave(YMatrix1,constraints,n,threads,path)
% Create figure
figure1 = figure;

% Create axes
axes1 = axes('Parent',figure1);
hold(axes1,'on');

% Create multiple lines using matrix input to plot
plot1 = plot(YMatrix1,'Color',[0 0 0],'Parent',axes1,'MarkerSize',8,...
    'LineWidth',1);

markers =['+','o','p','h','d','s','*','.','x','^','v','>','<'];

for i=1:length(threads)
    set(plot1(i),'DisplayName',sprintf('%d threads',threads(i)),'Marker',markers(i));
end
% 
% set(plot1(1),'DisplayName','2 threads','MarkerSize',6,'LineWidth',0.5);
% set(plot1(2),'DisplayName','4 threads','Marker','diamond');
% set(plot1(3),'DisplayName','8 threads','Marker','o');
% set(plot1(4),'DisplayName','16 threads','Marker','*');
% set(plot1(5),'DisplayName','32 threads','Marker','square');
% set(plot1(6),'DisplayName','64 threads','Marker','+');


% Create xlabel
xlabel('Increasing number of variables');

% Create title
% title(sprintf('Efficiency with problems fixed at %d constraints',constraints));

% Create ylabel
ylabel('Efficiency');

c = cell(length(constraints),1);
for i=1:length(constraints)
    c(i) = int2str(constraints(i));
end

% Uncomment the following line to preserve the Y-limits of the axes
ylim(axes1,[0 2.0]);
% Set the remaining axes properties
set(axes1,'XTick',[1:length(constraints)],'XTickLabel',...
   c,'YGrid','on','YMinorTick','on','YTick',...
    [0:0.1:2.0]);

hold(axes1,'off');
% Create legend
%legend(axes1,'show','Location','northeastoutside');
legend(axes1,'Location','northwest');

saveas(gcf,strcat(path,sprintf('/matlab_octave/graph/efficiency/Efficiency_%d_constraints_median_simplex.png',constraints(n))));


