function [ppl,f,A,b] = genrand(m,n,d)
    c = zeros(n,1);
    A =[];
    b = [];

    % Define the inline function as an anonymous function
    pl = @(x) (abs(x) + x) / 2;

    while c >=0
        %warning: inline is obsolete; use anonymous functions instead
        %new refactored code
        % pl=inline('(abs(x)+x)/2');

        A=sprand(m,n,d);
        % A=100*(A-0.5*spones(A));
        A=A*50;
        x=sparse(10*pl(rand(n,1)));
        u=spdiags((sign(pl(rand(m,1)-rand(m,1)))),0,m,m)*(rand(m,1)-rand(m,1));
        b=A*x;
        c=A'*u+spdiags((ones(n,1)-sign(pl(x))),0,n,n)*10*ones(n,1);
    end
    
    % Uncomment the following lines if you want to ensure that b is non-negative
    %negativos = find(b < 0);
    %b(negativos) = b(negativos)*-1;
    %A(negativos,:) = A(negativos,:)*-1;

    f = c';
    A = full(A);
    b = full(b);
    ppl = [A eye(m) b;f zeros(1,m+1)];
end