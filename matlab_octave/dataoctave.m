%# /usr/bin/octave-cli -qf
#! /opt/npad/shared/spack-v.0.21.0/opt/spack/linux-rocky8-haswell/gcc-8.5.0/octave-8.2.0-meiw63frywe75codials5viumjpdejw7/bin/octave-cli qf

% Add the path to your functions
addpath('/home/vrrcelestino/Paper_simplex/MulticoreParallelSimplex/matlab_octave'); 

arg_list = argv();

% Parse arguments
base_m = str2num(arg_list{1});
base_n = str2num(arg_list{2});
d = str2num(arg_list{3});

% Ensure the remaining arguments are parsed correctly
sizes = [];
for i = 4:length(arg_list)
    sizes = [sizes, str2num(arg_list{i})];
end

% Generate the initial problem
[problem, f, A, b] = genrand(base_m, base_n, d);

% Save the initial problem
save_problem(base_m, base_n, d, problem);

% Augment and save for subsequent sizes
for i = 1:2:length(sizes)
    m = sizes(i);
    n = sizes(i+1);
    
    % Check if augmentation is needed
    if m > base_m || n > base_n
        [A, b, f] = augmentMatrix(A, b, f, m - base_m, n - base_n, d);

        % Ensure f is a row vector for concatenation
        if iscolumn(f)
            f = f';  % Transpose f to a row vector
        end

        % Calculate total columns in the new row to be concatenated
        total_cols = size(A, 2) + size(A, 1) + 1;  % A columns + identity matrix columns + b column

        % Construct the problem matrix with the correct dimension adjustments
        problem = [A, eye(size(A, 1)), b; -f, zeros(1, total_cols - length(f))];

        % old code // problem = [A eye(size(A,1)) b; -f zeros(1, size(A,1)+1)];
        
        save_problem(m, n, d, problem);
    end
end
