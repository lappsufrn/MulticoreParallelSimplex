# 3D Plot with logarithm and different aspect ratios

from matplotlib.ticker import FormatStrFormatter

def log_tick_formatter(val, pos=None):
    return f'{np.exp(val):.2f}'

def create_efficiency_3d_plot_log(zdata1, thread, constraints, variables, path, \
                              elevation, azimuth, display_in_notebook=False):
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111, projection='3d')

    X, Y = np.meshgrid(np.log(constraints), np.log(variables))
    #Z = np.log(np.array(zdata1).T + 1)
    #X, Y = np.meshgrid(constraints, variables)
    Z = np.array(zdata1).T

    max_efficiency = np.max(Z)  # Find the maximum efficiency value
    z_axis_limit = max_efficiency + max_efficiency * 0.1  # Set z-axis limit to 10% above the max value

    surf = ax.plot_surface(X, Y, Z, cmap='viridis', edgecolor='none')
    # cbar = fig.colorbar(surf, shrink=0.5, aspect=5)
    # cbar.set_label('Efficiency', fontsize=14)

    ax.set_xlabel('Constraints', fontsize=14)
    ax.set_ylabel('Variables', fontsize=14)
    ax.set_zlabel('Efficiency', fontsize=14)
    ax.set_title(f'Efficiency with {thread} threads', fontsize=18)

    # Set limits and ticks for the axes
    ax.set_xlim(np.log(constraints[0]), np.log(constraints[-1]))
    ax.set_ylim(np.log(variables[0]), np.log(variables[-1]))
    #ax.set_xlim(constraints[0], constraints[-1])
    #ax.set_ylim(variables[0], variables[-1])
    ax.set_zlim(0, z_axis_limit)

    # Set tick labels to original scale
    ax.set_xticks(np.log(constraints))
    ax.set_xticklabels([str(c) for c in constraints])
    ax.set_yticks(np.log(variables))
    ax.set_yticklabels([str(v) for v in variables])
    #ax.set_xticks(constraints)
    #ax.set_yticks(variables)
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    #ax.zaxis.set_major_formatter(FuncFormatter(log_tick_formatter))

    # Adjust the viewing angle (elevation, azimuth)
    ax.view_init(elev=elevation, azim=azimuth)  # Example angles, adjust as needed

    if display_in_notebook:
        plt.show()
    else:
        plt.savefig(f'{path}/Efficiency3d_{thread}_threads_median_simplex.png', dpi=300)
    plt.close()
