import numpy as np

def save_problem(filename, A, b=None, c=None):
    with open(filename, 'w') as file:
        if b is not None and c is not None:
            np.savetxt(file, np.vstack([A, np.hstack([b, c])]), fmt='%.4f')
        else:
            np.savetxt(file, A, fmt='%.4f')
