# 3D Plot with logarithm, different aspect ratios and L3 cache boundary

from matplotlib.ticker import FormatStrFormatter

def log_tick_formatter(val, pos=None):
    return f'{np.exp(val):.2f}'

def create_efficiency_3d_plot_log_cache(zdata1, thread, constraints, variables, path, \
                              elevation, azimuth, display_in_notebook=False):
    fig = plt.figure(figsize=(12, 8))
    ax = fig.add_subplot(111, projection='3d')

    X, Y = np.meshgrid(np.log(constraints), np.log(variables))
    #Z = np.log(np.array(zdata1).T + 1)
    #X, Y = np.meshgrid(constraints, variables)
    Z = np.array(zdata1).T

    max_efficiency = np.max(Z)  # Find the maximum efficiency value
    z_axis_limit = max_efficiency + max_efficiency * 0.1  # Set z-axis limit to 10% above the max value


    #Calculate boundaries
    # Constants
    a = 1
    c = -268435456
    # Calculate the maximum number of constraints for each number of variables
    variables_array = np.array(variables)
    max_constraints = (-variables_array + np.sqrt(variables_array**2 - 4 * a * c)) / (2 * a)
    # Calculate the maximum number of variables for each number of constraints
    # constraints_array = np.array(constraints)
    # max_variables = (-constraints_array + np.sqrt(constraints_array**2 - 4 * a * c)) / (2 * a)


    # Plot the efficiency surface
    surf = ax.plot_surface(X, Y, Z, cmap='viridis', edgecolor='none')

    # Plot the boundary line
    # Convert max_constraints to the same scale as X and Y if they are in log scale
    boundary_X = np.log(variables)
    boundary_Y = np.log(max_constraints)
    boundary_Z = np.zeros_like(boundary_X)  # Assuming Z=0 for the boundary line, adjust as needed

    # # Calculate boundary_Z over the surface
    # # Flatten X, Y, Z for interpolation
    # points = np.transpose([X.flatten(), Y.flatten()])
    # values = Z.flatten()
    # # Prepare boundary points for interpolation
    # boundary_points = np.transpose([boundary_Y, boundary_X])
    # # Interpolate Z values at boundary points
    # from scipy.interpolate import griddata
    # boundary_Z = griddata(points, values, boundary_points, method='cubic') # method='linear' or 'cubic'


    # Plot the boundary line on the surface
    ax.plot(boundary_X, boundary_Y, boundary_Z, color='r', linewidth=2, label='L3 Cache Limit')

    # Adjust labels, limits, and viewing angle as needed

    ax.set_xlabel('Constraints', fontsize=14)
    ax.set_ylabel('Variables', fontsize=14)
    ax.set_zlabel('Efficiency', fontsize=14)
    ax.set_title(f'Efficiency with {thread} threads', fontsize=18)

    # Set limits and ticks for the axes
    ax.set_xlim(np.log(constraints[0]), np.log(constraints[-1]))
    ax.set_ylim(np.log(variables[0]), np.log(variables[-1]))
    #ax.set_xlim(constraints[0], constraints[-1])
    #ax.set_ylim(variables[0], variables[-1])
    ax.set_zlim(0, z_axis_limit)

    # Set tick labels to original scale
    ax.set_xticks(np.log(constraints))
    ax.set_xticklabels([str(c) for c in constraints])
    ax.set_yticks(np.log(variables))
    ax.set_yticklabels([str(v) for v in variables])
    #ax.set_xticks(constraints)
    #ax.set_yticks(variables)
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    #ax.zaxis.set_major_formatter(FuncFormatter(log_tick_formatter))

    # Adjust the viewing angle (elevation, azimuth)
    ax.view_init(elev=elevation, azim=azimuth)  # Example angles, adjust as needed

    if display_in_notebook:
        plt.show()
    else:
        plt.savefig(f'{path}/Efficiency3d_{thread}_threads_median_simplex.png', dpi=300)
    plt.close()
