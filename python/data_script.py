#! module load softwares/python/3.10.5-gnu8

import numpy as np
import scipy.sparse as sp
import sys

import genrand
import save_problem
import augment_matrix

def main():
    base_m = int(sys.argv[1])
    base_n = int(sys.argv[2])
    d = float(sys.argv[3])
    sizes = list(map(int, sys.argv[4:]))

    # Generate the initial problem
    A, b, c = genrand(base_m, base_n, d)
    save_problem(f"{base_m}x{base_n}_{d}.txt", A, b, c)

    # Augment and save for subsequent sizes
    idx = 0
    while idx < len(sizes):
        m = sizes[idx]
        n = sizes[idx + 1]
        A, b, c = augment_matrix(A, b, c, m - base_m, n - base_n, d)
        problem = np.vstack([np.hstack([A, np.eye(A.shape[0]), b[:, np.newaxis]]), np.hstack([-c, np.zeros(A.shape[0] + 1)])])
        save_problem(f"{m}x{n}_{d}.txt", problem)
        idx += 2

if __name__ == "__main__":
    main()
