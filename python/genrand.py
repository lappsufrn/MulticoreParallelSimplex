import numpy as np
import scipy.sparse as sp

def genrand(m, n, d):
    A = sp.rand(m, n, density=d, format='csr') * 50
    x = np.maximum(0, 10 * np.random.rand(n, 1))
    b = A.dot(x)
    u = np.random.rand(m, 1) * 2 - 1
    c = A.T.dot(u)
    return A, b, c.flatten()
