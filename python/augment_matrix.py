import numpy as np
import scipy.sparse as sp

def augment_matrix(A, b, c, add_m, add_n, d):
    A_new_rows = sp.rand(add_m, A.shape[1], density=d, format='csr') * 50
    A = sp.vstack([A, A_new_rows])
    b = np.vstack([b, sp.rand(add_m, 1, density=d).toarray() * 50])
    A_new_cols = sp.rand(A.shape[0], add_n, density=d, format='csr') * 50
    A = sp.hstack([A, A_new_cols])
    c = np.concatenate([c, np.zeros(add_n)])
    return A, b, c
