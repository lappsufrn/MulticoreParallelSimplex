import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

from create_efficiency_3d_plot_log.py import create_efficiency_3d_plot_log
from create_efficiency_3d_plot_log_cache.py import create_efficiency_3d_plot_log_cache

results = pd.read_csv("./results/results_medians_8192x8192.csv")

path = os.getcwd()

# Generate 3D Efficiency Plot - logarithmic with different views

# Define constraints, variables and threads
constraints = [256, 512, 1024, 2048, 4096, 8192, 16384]
variables = [256, 512, 1024, 2048, 4096, 8192, 16384]
threads = [2, 4, 8, 16, 32, 64, 128]

# Assuming zdata1 is a 3D array representing efficiency for different constraints and variables
for thread in threads:
    zdata1 = np.zeros((len(constraints), len(variables)))
    for i, constraint in enumerate(constraints):
        for j, variable in enumerate(variables):
            filtered_df = results[(results.constraints == constraint) &
                                        (results.variables == variable) &
                                        (results.numthreads == thread)]
            if not filtered_df.empty:
                zdata1[i, j] = filtered_df.efficiency.iloc[0]
            else:
                zdata1[i, j] = 0  # or some default value if no data is available
            #zdata1 = np.log(zdata1 + 1)
        #print(f'The maximum effiency for {thread} threads is {np.max(zdata1)}')
    create_efficiency_3d_plot_log(zdata1, thread, constraints, variables, path,
                              30, 250, display_in_notebook=True)

# Define constraints, variables and threads
constraints = [256, 512, 1024, 2048, 4096, 8192, 16384]
variables = [256, 512, 1024, 2048, 4096, 8192, 16384]
threads = [2, 4, 8, 16, 32, 64, 128]

# Assuming zdata1 is a 3D array representing efficiency for different constraints and variables
for thread in threads:
    zdata1 = np.zeros((len(constraints), len(variables)))
    for i, constraint in enumerate(constraints):
        for j, variable in enumerate(variables):
            filtered_df = results[(results.constraints == constraint) &
                                        (results.variables == variable) &
                                        (results.numthreads == thread)]
            if not filtered_df.empty:
                zdata1[i, j] = filtered_df.efficiency.iloc[0]
            else:
                zdata1[i, j] = 0  # or some default value if no data is available
            #zdata1 = np.log(zdata1 + 1)
        #print(f'The maximum effiency for {thread} threads is {np.max(zdata1)}')
    create_efficiency_3d_plot_log_cache(zdata1, thread, constraints, variables, path,
                              30, 250, display_in_notebook=True)