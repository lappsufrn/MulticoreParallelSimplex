CXX=g++
CXXFLAGS=-Ofast -Wall -msse2 -fopt-info-vec-optimized -ftree-vectorizer-verbose=2 -Wvector-operation-performance
BIN_DIR=$(CPP_DIR)/bin
CPP_DIR=cpp
INCLUDE_DIR=$(CPP_DIR)/include

all: serial parallel

serial: $(BIN_DIR)/mains $(BIN_DIR)/blocked_simplex_seq

parallel: $(BIN_DIR)/mainpnew $(BIN_DIR)/mainp $(BIN_DIR)/blocked_simplex_par

# serial

$(BIN_DIR)/mains: $(CPP_DIR)/sequential/mainS.cpp
	$(CXX) $(CXXFLAGS) $< -I$(INCLUDE_DIR) -o $@

$(BIN_DIR)/blocked_simplex_seq: $(CPP_DIR)/sequential/blocked_simplex_seq.cpp
	$(CXX) $(CXXFLAGS) $< -I$(INCLUDE_DIR) -o $@

# parallel

$(BIN_DIR)/mainpnew: $(CPP_DIR)/parallel/mainP_new_blocked.cpp
	$(CXX) $(CXXFLAGS) -fopenmp $< -I$(INCLUDE_DIR) -o $@

$(BIN_DIR)/mainp: $(CPP_DIR)/parallel/mainP.cpp
	$(CXX) $(CXXFLAGS) -fopenmp $< -I$(INCLUDE_DIR) -o $@

$(BIN_DIR)/blocked_simplex_par: $(CPP_DIR)/parallel/blocked_simplex_par.cpp
	$(CXX) $(CXXFLAGS) -fopenmp $< -I$(INCLUDE_DIR) -o $@

clean:
	rm -f $(BIN_DIR)/*
