#!/bin/bash
#SBATCH --time=2-00:00          #Tempo máximo do job no formato DIAS-HORAS:MINUTOS
#SBATCH --cpus-per-task=8 #128     #Quantidade de núcleos (nodes)
##SBATCH --exclusive             #Se cpus-per-task=128, deixe apenas # no início dessa linha (exclusive)
##SBATCH --hint=compute_bound    #use all cores in each socket
# #SBATCH --mail-user=vrcelestino@unb.br  #receber notificações por e-mail sobre início e fim de execução
# #SBATCH --mail-type=ALL
#SBATCH -p amd-512              #selecionar a partição

# Environment setup
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK  #fornece a você o número de cores que seu job terá durante a execução
. /opt/npad/shared/spack-v.0.21.0/share/spack/setup-env.sh
spack load octave@8.2.0

# Define paths
PATH_DATA=data
PATH_MATLAB=matlab_octave
#PATH_PYTHON=python

#Define matrix size in exponentials of 2
L1=8
L2=9
C1=8
C2=9

#Density factor for random matrix generation, between 0 (sparse) and 1 (dense)
d=0.9

# Prepare size arguments for dataoctave.m
size_args=""
for (( i = $L1; i <= $L2; i++ )); do
  for (( j = $C1; j <= $C2; j++ )); do
      m=$(echo 2^$i | bc)
      n=$(echo 2^$j | bc)
      size_args="$size_args $m $n"
  done
done

# Calculating powers using Bash and passing them to the Octave script
initial_m=$(echo "2^$L1" | bc)
initial_n=$(echo "2^$C1" | bc)

# Generating data
octave "$PATH_MATLAB/dataoctave.m" $initial_m $initial_n $d $size_args || { echo "Octave script failed"; exit 1; }

# Compress the generated files
find $PATH_DATA -maxdepth 1 -type f -name '*x*_*' -exec gzip "{}" \;







