#!/bin/bash
#SBATCH --time=7-00:00          #Tempo máximo do job no formato DIAS-HORAS:MINUTOS
#SBATCH --qos=qos2              #Estende de 2 para 7 dias o limite mãximo
#SBATCH --cpus-per-task=128     #Quantidade de núcleos (nodes)
#SBATCH --exclusive             #Se cpus-per-task=128, deixe apenas # no início dessa linha (exclusive)
#SBATCH --hint=compute_bound    #use all cores in each socket
#SBATCH --mail-user=vrcelestino@unb.br  #receber notificações por e-mail sobre início e fim de execução
#SBATCH --mail-type=ALL
#SBATCH -p amd-512               #selecionar a partição

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK  #fornece a você o número de cores que seu job terá durante a execução

##Para ver os módulos python existentes, use: module av

. /opt/npad/shared/spack-v.0.21.0/share/spack/setup-env.sh
#module load octave/8.2.0-gcc-8.5.0-meiw63f
spack load octave@8.2.0

PATH_PROJECT=~/Paper_simplex/MulticoreParallelSimplex
PATH_RESULTS=results
PATH_EXE=cpp
PATH_DATA=data
PATH_MATLAB=/matlab_octave
# PATH_CPLEX=cplex

# num_threads 1, 2, 4, 8, 16, 32, 64, 128, 256
NUM_THREADS=256

# Files 256x256_1 to 16384x16384_1
L1=8
L2=14
C1=8
C2=14

NUMB_OF_COL=`echo $C2-$C1+1 | bc`

# Files 256x256_1 to 16384x16384_1
K=1

# number of repetitions to gather average execution time
R=5

#D=0.9

#echo `rm results/out* results/filesseq`

# export GOMP_CPU_AFFINITY="0-3"

for (( i = $L1; i <= $L2; i++ )) do
  for (( j = $C1; j <= $C2; j++ )) do
    for (( k = 1; k <= $K; k++ )) do
      m=`echo 2^$i | bc`
      n=`echo 2^$j | bc`
      
      file=$PATH_DATA/$m"x"$n"_"$k
      echo $file
      echo "------------------" >> $PATH_RESULTS/out_256
      echo $file >> $PATH_RESULTS/out_256

      if [ -f "$file"".gz" ]
        then
          echo "$file found."

          echo `date +%d-%m-%Y:%H:%M:%S`": decompressing"
          echo `date +%d-%m-%Y:%H:%M:%S`": decompressing" >> $PATH_RESULTS/out_256

          gzip -d $file".gz"
        else
          echo "$file not found."
          
          echo `date +%d-%m-%Y:%H:%M:%S`": Generating data"
          echo `date +%d-%m-%Y:%H:%M:%S`": Generating data" >> $PATH_RESULTS/out_256

          echo `octave .$PATH_MATLAB/dataoctave.m $m $n $D $k`       
      fi 

      chunk=`echo $m+$n | bc`
      # Evaluate chunk size in performance for 0.20, 0.35, 0.50 in the future
      chunk=`echo $chunk*0.35 | bc`

      for (( r = 1; r <= $R; r++ )) do

        echo $file >> $PATH_RESULTS/filesseq_256

        for (( u = 1; u <= $NUM_THREADS; u*=2 )) do

          echo `date +%d-%m-%Y:%H:%M:%S`": parallel "$u" threads and chunk "$chunk
          echo `date +%d-%m-%Y:%H:%M:%S`": parallel "$u" threads and chunk "$chunk >> $PATH_RESULTS/out_256     
          temp=`./$PATH_EXE/bin/mainp $file $u $chunk $r`
          echo $temp >> $PATH_RESULTS/out_cpp_par_256.csv

          echo `date +%d-%m-%Y:%H:%M:%S`": sequential "$u" threads and chunk "$chunk
          echo `date +%d-%m-%Y:%H:%M:%S`": sequential "$u" threads and chunk "$chunk >> $PATH_RESULTS/out_256
          temp=`./$PATH_EXE/bin/mains $file $u $chunk $r`
          echo $temp >> $PATH_RESULTS/out_cpp_seq_256.csv

        done
      done
    gzip $file
    done
  done
done

# Graphs will be generated in Python instead
# echo `date +%d-%m-%Y:%H:%M:%S`": Generating graphs"
# echo `octave .$PATH_MATLAB/analysis_octave.m $L1 $L2 $NUMB_OF_COL $K $R $NUM_THREADS $PATH_PROJECT`

## Inclua no final do SSH -i C:\Users\cvict\AppData\Roaming\MobaXterm\home\.ssh\id_rsa