#!/bin/bash
#SBATCH --time=2-00:00          #Tempo máximo do job no formato DIAS-HORAS:MINUTOS
#SBATCH --qos=qos2              #Estende de 2 para 7 dias o limite máximo e limite de CPU por usuário
#SBATCH --cpus-per-task=128     #Quantidade de núcleos (nodes)
#SBATCH --exclusive             #Se cpus-per-task=128, deixe apenas # no início dessa linha (exclusive)
#SBATCH --hint=compute_bound    #use all cores in each socket
#SBATCH --mail-user=vrcelestino@unb.br  #receber notificações por e-mail sobre início e fim de execução
#SBATCH --mail-type=ALL
#SBATCH -p amd                  #selecionar a partição cluster, service, test, intel-256, intel-512, gpu, amd

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK  #fornece a você o número de cores que seu job terá durante a execução

##Para ver os módulos python existentes, use: module av

. /opt/npad/shared/spack-v.0.21.0/share/spack/setup-env.sh
#module load octave/8.2.0-gcc-8.5.0-meiw63f
spack load octave@8.2.0

PATH_PROJECT=~/Paper_simplex/MulticoreParallelSimplex
PATH_RESULTS=results
PATH_EXE=cpp
PATH_DATA=data
PATH_MATLAB=/matlab_octave
# PATH_CPLEX=cplex

# num_threads 1, 2, 4, 8, 16, 32, 64, 128, 256
NUM_THREADS=256

# Files 16x16_1 to 64x64_3
# L1=4
# L2=6
# C1=4
# C2=6
#L1=5
#L2=5
#C1=5
#C2=5

# Files 256x256_1 to 1024x1024_1
# L1=8
# L2=10
# C1=8
# C2=10

# Files 2048x2048_1 to 16384x16384_1
# L1=11
# L2=14
# C1=11
# C2=14

# Files 256x256_1 to 16384x16384_1 - 1st run on 11/11/2023
# L1=8
# L2=14
# C1=8
# C2=14

# File 1024x8192_1 - follow-up run on 12/11/2023
#L1=10
#L2=10
#C1=13
#C2=13

# Files 4096x8192_1 to 16384x16384_1 - follow-up run on 12/11/2023
#L1=12
#L2=14
#C1=13
#C2=14

# Files 8192x256_1 to 16384x4096_1 - follow-up run on 12/11/2023
#L1=13
#L2=14
#C1=8
#C2=12

# Files 1024x4096_1 to 1024x8192_1 - follow-up run on 14/11/2023
# L1=10
# L2=10
# C1=12
# C2=13

# Files 16834x256_1 to 16834x4096_1 - follow-up run on 21/11/2023
# L1=14
# L2=14
# C1=8
# C2=12

# Files 16834x256_1 to 16834x4096_1 - follow-up run on 22/11/2023
L1=14
L2=14
C1=10
C2=14

NUMB_OF_COL=`echo $C2-$C1+1 | bc`

# Files 16x16_1 to 64x64_3
# K=3 #number of different samples of the same size

# Files 256x256_1 to 1024x1024_1 and 2048x2048_1 to 16384x16384_1
K=1

# number of repetitions to gather average execution time
R=5

#D=0.9

#echo `rm results/out* results/filesseq` # follow-up run on 12/11/2023

for (( i = $L1; i <= $L2; i++ )) do
  for (( j = $C1; j <= $C2; j++ )) do
    for (( k = 1; k <= $K; k++ )) do
      m=`echo 2^$i | bc`
      n=`echo 2^$j | bc`
      
      file=$PATH_DATA/$m"x"$n"_"$k
      echo $file
      echo "------------------" >> $PATH_RESULTS/out3
      echo $file >> $PATH_RESULTS/out3

      if [ -f "$file"".gz" ]
        then
          echo "$file found."

          echo `date +%d-%m-%Y:%H:%M:%S`": decompressing"
          echo `date +%d-%m-%Y:%H:%M:%S`": decompressing" >> $PATH_RESULTS/out3

          gzip -d $file".gz"
        else
          echo "$file not found."
          
          echo `date +%d-%m-%Y:%H:%M:%S`": Generating data"
          echo `date +%d-%m-%Y:%H:%M:%S`": Generating data" >> $PATH_RESULTS/out3

          echo `octave .$PATH_MATLAB/dataoctave.m $m $n $D $k`       
      fi 

      chunk=`echo $m+$n | bc`
      # Evaluate chunk size in performance for 0.20, 0.35, 0.50
      chunk=`echo $chunk*0.35 | bc`

      for (( r = 1; r <= $R; r++ )) do

        echo $file >> $PATH_RESULTS/filesseq3

        for (( u = 1; u <= $NUM_THREADS; u*=2 )) do

          echo `date +%d-%m-%Y:%H:%M:%S`": parallel "$u" threads and chunk "$chunk
          echo `date +%d-%m-%Y:%H:%M:%S`": parallel "$u" threads and chunk "$chunk >> $PATH_RESULTS/out3 
          temp=`./$PATH_EXE/bin/mainp $file $u $chunk $r`
          echo $temp >> $PATH_RESULTS/out_cpp_par3.csv

          echo `date +%d-%m-%Y:%H:%M:%S`": sequential "$u" threads and chunk "$chunk
          echo `date +%d-%m-%Y:%H:%M:%S`": sequential "$u" threads and chunk "$chunk >> $PATH_RESULTS/out3 
          temp=`./$PATH_EXE/bin/mains $file $u $chunk $r`
          echo $temp >> $PATH_RESULTS/out_cpp_seq3.csv

        done
      done
    gzip $file
    done
  done
done


## Inclua no final do SSH -i C:\Users\cvict\AppData\Roaming\MobaXterm\home\.ssh\id_rsa