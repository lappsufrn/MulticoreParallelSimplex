#!/bin/bash
#SBATCH --time=2-00:00          #Tempo máximo do job no formato DIAS-HORAS:MINUTOS
#SBATCH --cpus-per-task=128     #Quantidade de núcleos (nodes)
#SBATCH --exclusive             #Se cpus-per-task=128, deixe apenas # no início dessa linha (exclusive)
#SBATCH --hint=compute_bound    #use all cores in each socket
# #SBATCH --mail-user=vrcelestino@unb.br  #receber notificações por e-mail sobre início e fim de execução
# #SBATCH --mail-type=ALL
#SBATCH -p amd-512              #selecionar a partição

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK  #fornece a você o número de cores que seu job terá durante a execução

##Para ver os módulos python existentes, use: module av
##module load octave/8.2.0-gcc-8.5.0-meiw63f
##module --ignore_cache load "octave/8.2.0-gcc-8.5.0-meiw63f"

#Load octave
. /opt/npad/shared/spack-v.0.21.0/share/spack/setup-env.sh
spack load octave@8.2.0

#PATH_PROJECT=~/Paper_simplex/MulticoreParallelSimplex
PATH_RESULTS=results
PATH_EXE=cpp
PATH_DATA=data
PATH_MATLAB=matlab_octave
PATH_PYTHON=python

#Define matrix size in exponentials of 2
L1=8
L2=14
C1=8
C2=14

#Define number of different random matrix with same size
K=1

#Density factor for random matrix generation, between 0 (sparse) and 1 (dense)
d=0.9

for (( i = $L1; i <= $L2; i++ )) do
  for (( j = $C1; j <= $C2; j++ )) do
    for (( k = 1; k <= $K; k++ )) do
      m=`echo 2^$i | bc`
      n=`echo 2^$j | bc`
      
      file=$PATH_DATA/$m"x"$n"_"$k
      echo $file

      if [ -f "$file"".gz" ]
      then
        echo "$file found."
      else
        echo "$file not found."
        
        #echo `date +%d-%m-%Y:%H:%M:%S`": Generating data"
        echo "$(date +%d-%m-%Y:%H:%M:%S): Generating data".

        #echo `octave $PATH_MATLAB/dataoctave.m $m $n $d $k`
        octave "$PATH_MATLAB/dataoctave.m" $m $n $d $k || { echo "Octave script failed"; exit 1; }

        gzip $file || { echo "Gzip failed"; exit 1; }
      fi 
    done
  done
done








