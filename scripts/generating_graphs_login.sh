#!/bin/bash

##Para ver os módulos python existentes, use: module av
. /opt/npad/shared/spack-v.0.21.0/share/spack/setup-env.sh
spack load octave@8.2.0


PATH_PROJECT=~/Paper_simplex/MulticoreParallelSimplex
PATH_RESULTS=results
PATH_EXE=cpp
PATH_DATA=data
PATH_MATLAB=matlab_octave
PATH_CPLEX=cplex

# num_threads 1, 2, 4, 8, 16, 32, 64, 128
NUM_THREADS=128

# Files 256x256_1 to 16384x16384_1
L1=8
L2=13
C1=8
C2=13

NUMB_OF_COL=`echo $C2-$C1+1 | bc`

K=1

R=1


# generating graphs
echo `date +%d-%m-%Y:%H:%M:%S`": Generating graphs"
echo `octave $PATH_MATLAB/analysis_octave.m $L1 $L2 $NUMB_OF_COL $K $R $NUM_THREADS $PATH_PROJECT`