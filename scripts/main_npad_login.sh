#!/bin/bash

##Para ver os módulos python existentes, use: module av
. /opt/npad/shared/spack-v.0.21.0/share/spack/setup-env.sh
module load octave/8.2.0-gcc-8.5.0-meiw63f

PATH_PROJECT=~/Paper_simplex/MulticoreParallelSimplex
PATH_RESULTS=results
PATH_EXE=cpp
PATH_DATA=data
PATH_MATLAB=/matlab_octave
#PATH_CPLEX=cplex

# num_threads 4, 8, 16, 32, 64, 128, 256
NUM_THREADS=2

L1=10
L2=10
C1=12
C2=13
NUMB_OF_COL=`echo $C2-$C1+1 | bc`
K=1
R=1

D=0.9

#echo `rm results/out* results/filesseq`

#export GOMP_CPU_AFFINITY="0-3"

for (( i = $L1; i <= $L2; i++ )) do
  for (( j = $C1; j <= $C2; j++ )) do
    for (( k = 1; k <= $K; k++ )) do
      m=`echo 2^$i | bc`
      n=`echo 2^$j | bc`
      
      file=$PATH_DATA/$m"x"$n"_"$k
      echo $file
      echo "------------------" >>$PATH_RESULTS/out
      echo $file >> $PATH_RESULTS/out

      if [ -f "$file"".gz" ]
        then
          echo "$file found."

          echo `date +%d-%m-%Y:%H:%M:%S`": decompressing"
          echo `date +%d-%m-%Y:%H:%M:%S`": decompressing" >> $PATH_RESULTS/out

          gzip -d $file".gz"
        else
          echo "$file not found."
          
          echo `date +%d-%m-%Y:%H:%M:%S`": Generating data"
          echo `date +%d-%m-%Y:%H:%M:%S`": Generating data" >> $PATH_RESULTS/out

          #echo `.$PATH_MATLAB/dataoctave.m $m $n $D $k`
          echo `octave $PATH_MATLAB/dataoctave.m $m $n $d $k`       
      fi 

      chunk=`echo $m+$n | bc`
      # Evaluate chunk size in performance for 0.20, 0.35, 0.50
      chunk=`echo $chunk*0.35 | bc`

      for (( r = 1; r <= $R; r++ )) do

        echo $file >> $PATH_RESULTS/filesseq

        for (( u = 1; u <= $NUM_THREADS; u*=2 )) do

          echo `date +%d-%m-%Y:%H:%M:%S`": parallel "$u" threads and chunk "$chunk
          echo `date +%d-%m-%Y:%H:%M:%S`": parallel "$u" threads and chunk "$chunk >> $PATH_RESULTS/out        
          temp=`./$PATH_EXE/bin/mainp $file $u $chunk $r`
          echo $temp >> $PATH_RESULTS/out_cpp_par.csv

          echo `date +%d-%m-%Y:%H:%M:%S`": sequential "$u" threads and chunk "$chunk
          echo `date +%d-%m-%Y:%H:%M:%S`": sequential "$u" threads and chunk "$chunk >> $PATH_RESULTS/out        
          temp=`./$PATH_EXE/bin/mains $file $u $chunk $r`
          echo $temp >> $PATH_RESULTS/out_cpp_seq.csv

        done
      done
    gzip $file
    done
  done
done

#echo `date +%d-%m-%Y:%H:%M:%S`": Generating graphs"
#echo `octave .$PATH_MATLAB/analysis_octave.m $L1 $L2 $NUMB_OF_COL $K $R $NUM_THREADS $PATH_PROJECT`