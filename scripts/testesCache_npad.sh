#!/bin/bash
#SBATCH --time=0-01:30          #Tempo máximo do job no formato DIAS-HORAS:MINUTOS
#SBATCH --cpus-per-task=32      #Quantidade de núcleos (nodes)
#SBATCH --exclusive             #Se cpus-per-task=32, deixe apenas # no início dessa linha (exclusive)
#SBATCH --hint=compute_bound    #use all cores in each socket
#SBATCH --mail-user=vrcelestino@unb.br  #receber notificações por e-mail sobre início e fim de execução
#SBATCH --mail-type=ALL
#SBATCH -p amd-512               #selecionar a partição

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK  #fornece a você o número de cores que seu job terá durante a execução

##Para ver os módulos python existentes, use: module av
#module load 
module load valgrind/3.18.1

PATH_PROJECT=~/Paper_simplex/MulticoreParallelSimplex
PATH_RESULTS=results
PATH_EXE=cpp
PATH_DATA=data
PATH_MATLAB=/matlab_octave
PATH_CPLEX=cplex
NUM_THREADS=4

L1=5
L2=5
C1=5
C2=5
NUMB_OF_COL=`echo $C2-$C1+1 | bc`
K=1
R=1

D=0.9

# export GOMP_CPU_AFFINITY="0-3"

for (( i = $L1; i <= $L2; i++ )) do
  for (( j = $C1; j <= $C2; j++ )) do
    for (( k = 1; k <= $K; k++ )) do
      m=`echo 2^$i | bc`
      n=`echo 2^$j | bc`
      
      file=$PATH_DATA/$m"x"$n"_"$k
      echo $file

      if [ -f "$file"".gz" ]
        then
          echo "$file found."

          echo `date +%d-%m-%Y:%H:%M:%S`": decompressing"

          gzip -d $file".gz"
        else
          echo "$file not found."
      fi

      chunk=`echo $m+$n | bc`
      chunk=`echo $chunk*0.35 | bc`

      for (( r = 1; r <= $R; r++ )) do

        for (( u = 2; u <= $NUM_THREADS; u*=2 )) do

          echo `date +%d-%m-%Y:%H:%M:%S`": parallel simplex running with "$u" threads and chunk "$chunk       
          temp=`valgrind --tool=cachegrind ./$PATH_EXE/bin/mainp $file $u $chunk`

          echo `date +%d-%m-%Y:%H:%M:%S`": sequential simplex running with "$u" threads and chunk "$chunk       
          temp=`valgrind --tool=cachegrind ./$PATH_EXE/bin/mains $file $u $chunk`

        done
      done      

    gzip $file
    done
  done
done

## Inclua no final do SSH -i C:\Users\cvict\AppData\Roaming\MobaXterm\home\.ssh\id_rsa

