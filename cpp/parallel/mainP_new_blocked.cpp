// ----------------------------------------------------------------------------
/**
 * @file  mainP.cpp
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    01/2018
 *
 * @brief This file contains a multicore parallel implementation of the standard
 *  simplex algorithm for solving linear programming problems using OpenMP.
 * @language: C++
 *
 * @section Description
 *  The current implementation uses the standard simplex algorithm in tabular
 *  form to parallelize. The LP(Linear Programming) problem needs to be modeled
 *  in the standard form and can not have artificial variables.
 *
 *  Standard form:
 *  maximize z = c^t * x,
 *  Subject to Ax = b,
 *             x >= 0
 *
 *
 * @subsection Input Data
 *
 *  The input data is a file composed of the constraint matrix 'A', the vector of
 *  coefficients of the objective function 'c' and the vector of independent values 'b',
 *  as shown below. So, the independent values are in the last column and the objectives
 *  values are in the last row.
 *
 *  | A  b|
 *  |-c  0|
 *
 * @subsection Compilation
 *   To compile this file you need to run the following command:
 *
 *  g++ -Ofast simplexParallel.cpp -fopenmp -Wall -msse2 -fopt-info-vec-optimized  -ftree-vectorizer-verbose=2  -Wvector-operation-performance -o exec_name
 *
 * @subsection usage
 *   To run execute the command:
 *
 *   ./exec_name /PATH/name_of_input_file numb_of_threads chunk
 *
 *   chunk is a positive integer that specifies a chunk size of a chunk-sized block of loop
 *   iterations to give to each thread.
 *   The file's name needs to be like: number_of_constraintxnumber_of_variables.
 *   Example of a usage with a 2000x2000 LP problem, 16 threads and a chunk of 1000:
 *
 *   ./exec_name 2000x2000 16 1000
 *
 *  
 * @subsection development history
 * Proposed by Felipe using more complex blocked simplex - still in development
 * 
 */
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <cstdio>
#include <omp.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <cstring>
#include <string>

#include "time.h"


////////// DEFINE UTILS /////////
#define B 2
#define C 2
/////////////////////////////////


using namespace std;

double *tableau;
ofstream log_file;

struct Compare_Max {
    double val = 0;
    int index = -1;
};

struct Compare_Min {
    double val = HUGE_VAL;
    int index = -1;
};
#pragma omp declare reduction(minimo : struct Compare_Min : omp_out = omp_in.val < omp_out.val ? omp_in : omp_out)
#pragma omp declare reduction(maximo : struct Compare_Max : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)

/**
 * Alocate matrix
 * @param nL
 * @param nC
 * @return a double pointer to pointer
 */
double * alocate_matrix(int nL, int nC) {

    double *tableau = new (nothrow) double [nL * nC];

    if (tableau == 0) {
        cerr << "Não foi possível alocar matriz \n";
        exit(EXIT_FAILURE);
    }

    return tableau;
}

/**
 * Delete matrix
 * @param tableau
 * @param nL
 */
void delete_matrix(double *tableau, int nL) {

    if (tableau == 0) return;


    delete [] tableau;
}

/**
 * Calculate the time spent between a start and a end timespec structure.
 * @param start
 * @param end
 * @return
 */
struct timespec My_diff(struct timespec start, struct timespec end) {
    struct timespec temp;

    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}

/**
 * Aux function to convert string to double
 * @param t
 * @param s
 * @param f
 * @return
 */
template <class T>
bool from_string(T& t, const string& s, ios_base& (*f)(ios_base&)) {
    istringstream iss(s);
    return !(iss >> f >> t).fail();
}

/**
 * Function to convert the string line coefficients to double
 * @param s
 * @return
 */
template <class T>
vector<T> string_to_vector(string s) {
    istringstream iss(s);
    vector<T> vec;
    do {
        string sub;
        iss >> sub;
        T numb;
        if (from_string<T>(numb, sub, dec)) {
            vec.push_back(numb);
        }
    } while (iss);
    return vec;
}

/**
 * Get the dimension of matrix from file name.
 * @param argv
 * @param nL
 * @param nC
 */
void get_dimension(char** argv, int &nL, int &nC) {
    int dimension[2] = {0, 0}, i = 0;
    //QUANDO FOR EXECUTAR DENTRO DA PASTA CPP INCREMENTE I DENTRO DO IF
    char *pch = strtok(argv[1], "x/_");
    while (pch != NULL) {

        if (i == 1) {
            dimension[0] = atoi(pch);
        }
        if (i == 2) {
            dimension[1] = atoi(pch);
        }
        pch = strtok(NULL, "x/_");
        i++;
    }
    nL = dimension[0] + 1;
    nC = dimension[0] + dimension[1] + 1;
}

/**
 * Function to read from the file the information needed for simplex algorithm.
 * @param argv
 * @param nL
 * @param nC
 * @return
 */
double * read_data(char** argv, int& nL, int& nC) {

    ifstream file(argv[1]);

    log_file.open("log_cpp", ofstream::app);

    if (!file.is_open()) {
        cerr << "Error opening file";
        exit(1);
    }

    get_dimension(argv, nL, nC);

    log_file << "----" << nL << "x" << nC << "----" << endl << endl;

    double * tableau = alocate_matrix(nL, nC);

    int lin = 0;
    string line;
    vector<int> sizes_col;
    while (getline(file, line)) {

        if (line == "") break;

        vector<double> contraint = string_to_vector<double>(line);

        copy(contraint.begin(), contraint.end(), &tableau[lin*nC]);

        lin++;
        sizes_col.push_back(contraint.size());
    }

    log_file << "lin " << lin << endl << "col:\n";

    for (uint i = 0; i < sizes_col.size(); i++) {
        log_file << i << " : " << sizes_col[i] << endl;
    }


    return tableau;
}

/**
 * Main function where is implemented the parallel simplex
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char** argv) {
    int numbThreads, constraintNumb, colNumb, ni = 0, chunk = 1;
    int count = 0, count2 = 0;

    tableau = read_data(argv, constraintNumb, colNumb);

    constraintNumb--;
    colNumb--;

    from_string<int>(numbThreads, string(argv[2]), std::dec);

    omp_set_num_threads(numbThreads);
    //-----
    log_file << "numbThreads " << numbThreads << endl;

    ni = 0;

    struct timespec timeTotalInit, timeTotalEnd;

    from_string<int>(chunk, string(argv[3]), std::dec);
    log_file << "chunk " << chunk << endl;

    struct Compare_Max max;
    struct Compare_Min min;

    if (clock_gettime(CLOCK_REALTIME, &timeTotalInit)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

#pragma omp parallel default(none) shared(min,max,chunk,numbThreads,count,tableau,count2,colNumb,constraintNumb,ni)
    {
        double pivot, pivot2, pivot3;
        int i, j;

// STEP 2
#pragma omp for schedule(guided,chunk) reduction(maximo:max)
        for (j = 0; j <= colNumb; j++)
            if (tableau[constraintNumb * (constraintNumb + 1) + j] < 0.0 && max.val < (-tableau[constraintNumb * (constraintNumb+1) + j])) {
                max.val = -tableau[constraintNumb * (constraintNumb + 1) + j];
                max.index = j;
            }

#pragma omp single nowait
        max.val = 0;

        do {

// STEP 3
#pragma omp for reduction(+:count),reduction(minimo:min) schedule(guided,chunk)
            for (i = 0; i < constraintNumb; i++) {
                if (tableau[i * (colNumb + 1)  + max.index] > 0.0) {
                    pivot = tableau[i * (colNumb + 1) + colNumb] / tableau[i * (colNumb + 1) + max.index];
                    if (min.val > pivot) {
                        min.val = pivot;
                        min.index = i;
                    }
                } else
                    count++;
            }

#pragma omp single nowait
            {
                if (count == constraintNumb) {
                    printf("Solution not found\n");
                    exit(1);
                } else
                    count = 0;

                count2 = 0;
            }

            pivot = tableau[min.index * (colNumb + 1) + max.index];
            pivot3 = -tableau[constraintNumb * (colNumb + 1) + max.index];

#pragma omp barrier
// STEP 4
#pragma omp for
            for (j = 0; j <= (colNumb); j++) {
                tableau[min.index* (colNumb + 1) + j] = tableau[min.index * (colNumb + 1) + j] / pivot;
            }

 double coefficients[constraintNumb];

#pragma omp for
    for(i = 0; i < constraintNumb; i++)
    {
      coefficients[i] = tableau[(i * colNumb)+ (max.index)];
    }

// STEP 5
/*

*/

for(int i = constraintNumb + 1 - C, kappa = 0, j = 0; i >= 0; i -= C, kappa++) {
  for(int k = 0; k < colNumb + 1; k += B) {

    #pragma omp for nowait
    for(int ii = 0; ii < C; ii++) {
      for(int jj = 0; jj < B; jj++) {
        if(i + ii == min.index)
          continue;

        tableau[(i + ii)%(constraintNumb+1) * colNumb + (j + jj)] -= tableau[min.index * (constraintNumb+1) + (j + jj)%(colNumb+1)] * coefficients[(i + ii)%(constraintNumb+1)] / pivot;

      }
    }
    j += B * (kappa%2 == 0? 1: -1);
    #pragma omp barrier
  }
  j -= B * (kappa%2 == 0? 1: -1);
}
/*
#pragma omp for nowait
            for (i = 0; i < constraintNumb; i++) {
                if (i != min.index) {
                    pivot2 = -tableau[i * (colNumb + 1) + max.index];
#pragma GCC ivdep
                    for (j = 0; j <= colNumb; j++) {
                        tableau[i * (colNumb + 1) + j] += (pivot2 * tableau[min.index * (colNumb + 1) + j]);
                    }
                }
            }
*/
// STEP 6
#pragma omp for reduction(+:count2),reduction(maximo:max) schedule(guided,chunk)
            for (j = 0; j <= colNumb; j++) {
                tableau[constraintNumb * (colNumb + 1) + j] += (pivot3 * tableau[min.index * (colNumb + 1) + j]);
                if (j < colNumb && tableau[constraintNumb * (colNumb + 1) + j] < 0.0) {
                    count2++;
                    if (max.val < (-tableau[constraintNumb * (colNumb + 1) + j])) {
                        max.val = -tableau[constraintNumb * (colNumb + 1) + j];
                        max.index = j;
                    }
                }
            }

#pragma omp single
            {
                ni++;
                max.val = 0.0;
                min.val = HUGE_VAL;
            }
        } while (count2);
    }


    if (clock_gettime(CLOCK_REALTIME, &timeTotalEnd)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    double ONE_SECOND_IN_NANOSECONDS = 1000000000;
    struct timespec time = My_diff(timeTotalInit, timeTotalEnd);
    double processTime = (time.tv_sec + (double) time.tv_nsec / ONE_SECOND_IN_NANOSECONDS);

    printf("%f %f ", processTime / ni, processTime);
    printf("%d %f \n", ni, tableau[constraintNumb * (colNumb + 1) + colNumb]);

    delete_matrix(tableau, constraintNumb);
    log_file.close();
}
