// ----------------------------------------------------------------------------
/**
 * @file  mainP.cpp
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    01/2018
 * 
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 *
 * @brief This file contains a multicore parallel implementation of the standard
 *  simplex algorithm for solving linear programming problems using OpenMP.
 * 
 * @language: C++
 *
 * @section Description
 *  The current implementation uses the standard simplex algorithm in tabular
 *  form to parallelize. The LP(Linear Programming) problem needs to be modeled
 *  in the standard form and can not have artificial variables.
 *
 *  Standard form:
 *  maximize z = c^t * x,
 *  Subject to Ax = b,
 *             x >= 0
 *
 *
 * @subsection Input Data
 *
 *  The input data is a file composed of the constraint matrix 'A', the vector of
 *  coefficients of the objective function 'c' and the vector of independent values 'b',
 *  as shown below. So, the independent values are in the last column and the objectives
 *  values are in the last row.
 *
 *  | A  b|
 *  |-c  0|
 *
 * @subsection Compilation
 *   To compile this file you need to run the following command:
 *
 *  g++ -I./cpp/include -Ofast simplexParallel.cpp -fopenmp -Wall -msse2 -fopt-info-vec-optimized  -ftree-vectorizer-verbose=2  -Wvector-operation-performance -o exec_name
 *
 * @subsection usage
 *   To run execute the command:
 *
 *   ./exec_name /PATH/name_of_input_file numb_of_threads chunk
 *
 *   chunk is a positive integer that specifies a chunk size of a chunk-sized block of loop
 *   iterations to give to each thread.
 *   The file's name needs to be like: number_of_constraintxnumber_of_variables.
 *   Example of a usage with a 2000x2000 LP problem, 16 threads and a chunk of 1000:
 *
 *   ./exec_name 2000x2000 16 1000
 *
 *  *
 * @subsection development history
 * Proposed by Victor from mainPbckp.cpp - under test and verification
 * 
 */
// ----------------------------------------------------------------------------

#include <stdlib.h>
#include <cstdio>
#include <omp.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <cstring>
#include <string>

#include <chrono>
#include <ctime>
#include "time.h"
#include "functions.h"
#include "openmp_utils.h"
// #include "SimplexSteps.h"

using namespace std;

double **tableau;

// REPLACED by openmp_utils.h
// struct Compare_Max {
//     double val = 0;
//     int index = -1;
// };

// struct Compare_Min {
//     double val = HUGE_VAL;
//     int index = -1;
// };

// #pragma omp declare reduction(minimo : struct Compare_Min : omp_out = omp_in.val < omp_out.val ? omp_in : omp_out)
// #pragma omp declare reduction(maximo : struct Compare_Max : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)

/**
 * Main function where is implemented the parallel simplex
 * @param argc
 * @param argv
 * @return
 * 
 */

int main(int argc, char** argv) {
    int nRow, nCol, numbThreads, chunk, ni = 0;
    int count = 0; 
    int count2 = 0;

    // Read tableau data
    tableau = read_data(argv[1], nRow, nCol);

    // Update log file
    // Declare a variable named log_file of the type ofstream (Output File Stream).
    ofstream log_file;

    log_file.open("./logs/log_par",ofstream::app);

    if(!log_file.is_open()) {
    cerr << "Error: Could not open log file." << endl;
    return 1; // or another appropriate error code
    }

    log_file << "parallel simplex" << endl;
    log_file << "filename: " << argv[1] << endl;
    log_file << "----" << nRow << "x" << nCol <<"----"<< endl;
    log_file << "nRow: " << nRow << endl;
    log_file << "nCol: " << nCol << endl;

    // Adjust initial row and column to zero (0)
    nRow--;
    nCol--;

    from_string<int>(numbThreads, string(argv[2]), std::dec);
    omp_set_num_threads(numbThreads); 
    log_file << "numbThreads: " << numbThreads << endl;

    from_string<int>(chunk, string(argv[3]), std::dec);
    log_file << "chunk: " << chunk << endl;

    // Get the current time as a timespec
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts)) {
    perror("Error in retrieving system time");
    exit(EXIT_FAILURE);
    }

    // Convert the timespec to a time_point
    chrono::time_point<chrono::system_clock> tp =
        chrono::system_clock::from_time_t(ts.tv_sec) +
        chrono::nanoseconds(ts.tv_nsec);

    // Get the date and time components from the time_point
    time_t tt = chrono::system_clock::to_time_t(tp);
    struct tm tm;
    localtime_r(&tt, &tm); // Use localtime_r on POSIX systems
    int year = tm.tm_year + 1900;
    int month = tm.tm_mon + 1;
    int day = tm.tm_mday;
    int hour = tm.tm_hour;
    int minute = tm.tm_min;
    int second = tm.tm_sec;

    // Print the date and time in a formatted way
    log_file << "The current date and time is: " << year << "-" << month << "-"
                << day << " " << hour << ":" << minute << ":" << second << "."
                << "\n";


    // // For debugging
    // cout << "filename: " << argv[1] << endl;
    // cout << "----" << nRow + 1 << "x" << nCol + 1 <<"----"<< endl;
    // cout << "nRow: " << nRow << " nCol: " << nCol << endl;


    // Initialize counting number of iterations in simplex algorithm
    ni = 0;

    struct timespec timeTotalInit, timeTotalEnd;

    struct Compare_Max max;
    struct Compare_Min min;

    if (clock_gettime(CLOCK_REALTIME, &timeTotalInit)) {
    perror("Error in retrieving system time");
    exit(EXIT_FAILURE);
    }

    // #pragma omp parallel default(none) shared(min,max,chunk,numbThreads,tableau,nRow,nCol,ni,count2)
    #pragma omp parallel default(none) shared(min,max,chunk,numbThreads,count,tableau,count2,nRow,nCol,ni)
    {

        double pivot, pivot3;
        double pivot2;
        int i, j;

        // STEP 2
        // Compare_Max max = findMaxObjective(tableau, nRow, nCol);
        #pragma omp for schedule(guided,chunk) reduction(maximo:max)
            for (j = 0; j <= nCol; j++)
                if (tableau[nRow][j] < 0.0 && max.val < (-tableau[nRow][j])) {
                    max.val = -tableau[nRow][j];
                    max.index = j;
                }

        #pragma omp single nowait
        max.val = 0;

        do {

        // STEP 3
        // Compare_Min min = findPivotRow(tableau, nRow, nCol, max.index, &count2);
        #pragma omp for reduction(+:count),reduction(minimo:min) schedule(guided,chunk)
            for (i = 0; i < nRow; i++) {
                if (tableau[i][max.index] > 0.0) {
                    pivot = tableau[i][nCol] / tableau[i][max.index];
                    if (min.val > pivot) {
                        min.val = pivot;
                        min.index = i;
                    }
                } else
                    count++;
            }

        #pragma omp single nowait
           {
               if (count == nRow) {
                   printf("Solution not found\n");
                   exit(1);
               } else
                   count = 0;

               count2 = 0;
           }

        pivot = tableau[min.index][max.index];
        pivot3 = -tableau[nRow][max.index];

        // Barrier not more needed (pivot, pivot2, pivot3 not more shared)
        #pragma omp barrier

        // STEP 4
        // updateRow(tableau, nRow, nCol, min.index, max.index);
        #pragma omp for
                for (j = 0; j <= (nCol); j++) {
                    tableau[min.index][j] = tableau[min.index][j] / pivot;
                }

        // STEP 5
        // performRowOperations(tableau, nRow, nCol, min.index, max.index);
        #pragma omp for nowait
                //Note that update of last row of tableau is delayed to STEP 6
                for (i = 0; i < nRow; i++) {
                    if (i != min.index) {
                        pivot2 = -tableau[i][max.index];
         #pragma GCC ivdep
                        for (j = 0; j <= nCol; j++) {
                            tableau[i][j] = (pivot2 * tableau[min.index][j]) + tableau[i][j];
                        }
                    }
                }

        // STEP 6
        // Compare_Max max = updateObjective(tableau,nRow,nCol,min.index,max.index,&count2);
         #pragma omp for reduction(+:count2),reduction(maximo:max) schedule(guided,chunk)
                    for (int j = 0; j <= nCol; j++) {
                        tableau[nRow][j] = (pivot3 * tableau[min.index][j]) + tableau[nRow][j];
                if (j < nCol && tableau[nRow][j] < 0.0) {
                    count2++;
                    if (max.val < (-tableau[nRow][j])) {
                        max.val = -tableau[nRow][j];
                        max.index = j;
                    }
                }
            }

        #pragma omp single
            {
                ni++;
                max.val = 0.0;
                min.val = HUGE_VAL;
            }

        } while (count2);
    }

    if (clock_gettime(CLOCK_REALTIME, &timeTotalEnd)) {
        perror("Error in retrieving system time");
        exit(EXIT_FAILURE);
    }

    double NANOSECONDS_IN_ONE_SECOND = 1000000000;
    struct timespec time = My_diff(timeTotalInit, timeTotalEnd);
    double processTime = (time.tv_sec + (double) time.tv_nsec / NANOSECONDS_IN_ONE_SECOND);

    // printf("%s %s %s %s %s %s %s %s %s %s %s \n", "filename ","nRow ","nCol ","numthreads ",
    // "chunk ","ni ","processtime/ni ","processtime ", "obj_value", "run", "method");
    printf("%s %d % d %s %s %d %.9f %.9f %f %s %s \n", argv[1], nRow +1, nCol +1, argv[2], argv[3], ni,
    processTime / ni, processTime, tableau[nRow][nCol], argv[4], "parallel");
    // printf("%f %f ", processTime / ni, processTime);
    // printf("%d %f \n", ni, tableau[nRow][nCol]);

    // printMatrix(tableau, nRow, nCol);

    log_file << "processTime / ni: " << processTime / ni << endl;
    log_file << "processTime: " << processTime  << endl;
    log_file << "no of iterations (ni): " << ni  << endl;
    log_file << "Optimal value: " << tableau[nRow][nCol]  << endl;

    log_file.close();

    if(log_file.fail()) {
    cerr << "Error: Could not close log file." << endl;
    return 1; // or another appropriate error code
    }

    delete_matrix(tableau, nRow);

}
