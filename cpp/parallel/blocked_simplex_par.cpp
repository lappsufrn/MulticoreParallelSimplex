// ----------------------------------------------------------------------------
/**
 * @file  mainS.cpp
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    01/2018
 *
 * @brief This file contains a sequecolSizeial implementation of the standard
 *  simplex algorithm for solving linear programming problems using OpenMP.
 * @language: C++
 *
 * @section Description
 *  The current implementation uses the standard simplex algorithm in tabular
 *  form. The LP(Linear Programming) problem needs to be modeled
 *  in the standard form and can not have artificial variables.
 *
 *  Standard form:
 *  maximize z = c^t * x,
 *  Subject to Ax = b,
 *             x >= 0
 *
 *
 * @subsection Input Data
 *
 *  The input data is a file composed of the constraint matrix 'A', the vector of
 *  coefficients of the objective fucolSizetion 'c' and the vector of independent values 'b',
 *  as shown below. So, the independent values are in the last column and the objectives
 *  values are in the last row.
 *
 *  | A  b|
 *  |-c  0|
 *
 * @subsection Compilation
 *   To compile this file you need to run the following command:
 *
 *  g++ -Ofast simplexParallel.cpp -fopenmp -Wall -msse2 -fopt-info-vec-optimized  -ftree-vectorizer-verbose=2  -Wvector-operation-performacolSizee -o exec_name
 *
 * @subsection usage
 *   To run execute the command:
 *
 *   ./exec_name /PATH/name_of_input_file numb_of_threads chunk
 *
 *   chunk is a positive integer that specifies a chunk size of a chunk-sized block of loop
 *   iterations to give to each thread.
 *   The file's name needs to be like: number_of_constraintxnumber_of_variables.
 *   Example of a usage with a 2000x2000 LP problem, 16 threads and a chunk of 1000:
 *
 *   ./exec_name 2000x2000 16 1000
 *
 * @subsection development history
 * Proposed by Felipe using blocked simplex - still in development
 * 
 */
// ----------------------------------------------------------------------------

#include "newfunctions.h"
#include <iostream>
#include <cmath>
#include <stdio.h>
#include <omp.h>

using namespace std;

double* tableau;

struct Compare_Max {
    double val = -1;
    int index = -1;
};

struct Compare_Min {
    double val = HUGE_VAL;
    int index = -1;
};
#pragma omp declare reduction(minimo : struct Compare_Min : omp_out = omp_in.val < omp_out.val ? omp_in : omp_out)
#pragma omp declare reduction(maximo : struct Compare_Max : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)

void printMatrix(double* v, int nL, int nC)
{
  for (int i = 0 ; i < nL; i++)
  {
    for (int j = 0; j < nC; j++)
    {
      cout << v[i * nC + j] << " ";
    }
    cout << endl;
  }
}

int main(int argc, char** argv) {

    int rowSize, colSize, ni = -1;
    int negatives, count;
    double maximo = 0, minimo = 1, pivot;

    Compare_Max pivot_col;
    Compare_Min pivot_row;

    struct timespec timeTotalInit, timeTotalEnd;

    tableau = read_data(argv, rowSize, colSize);

    int B, num_th;

    B = atoi(argv[2]);
    num_th = atoi(argv[3]);
/*
    rowSize--;
    colSize--;
*/

    if (clock_gettime(CLOCK_REALTIME, &timeTotalInit)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    int lastRow = rowSize - 1;
    int lastCol = colSize - 1;

    omp_set_num_threads(num_th);

#pragma omp parallel shared(lastRow, lastCol, rowSize, colSize, pivot_row, pivot_col) private(pivot)
{
    do {

#pragma omp single
{
      pivot_col.val = -1;
      pivot_col.index = -1;
      count = 0;

      negatives = 0;
      pivot_row.val = HUGE_VAL;
      pivot_row.index = -1;

      ni++;
//      printf("--- Thread: %d -- COMECO DA ITERACAO %d ---\n\n", omp_get_thread_num(),ni);
}

      // SELECT THE COLUMN INDEX WITH THE MINIMUM
      // NEGATIVE VALUE OF THE OBJECTIVE ROW
//printf("%d \t debug\n", omp_get_thread_num());
#pragma omp for reduction(+:count) reduction(maximo: pivot_col)
      for(int j = 0; j < lastCol; j++)
      {
        double aux =  tableau[lastRow * colSize + j];
        if( aux < 0)
        {
          if (-aux > pivot_col.val)
          {
            pivot_col.val = -aux;
            pivot_col.index = j;
          }
        }
        else
          count++;
      }

      if ( count == lastCol)
        break;


// printf("ni = %d\ttid: %d \t  pivot_col -> val: %f \t index : %d \n" ,ni,omp_get_thread_num(), pivot_col.val, pivot_col.index);
      // VARIAVEL COMPARTILHADA
      // TODO REDUCTION EM UMA UNICA VARIÁVEL SERÁ COMPARTILHADO

      // FOR PARALELO
      // ESSE LAÇO VAI LÁ PRA CIMA
/*      for (int i = 0; i < lastCol; i++)
        if ( tableau[lastRow * colSize + i] >= 0)
          count++;
*/      // SINGLE

      // SELECT THE ROW INDEX WITH THE
      // MINIMUM RATIO

      // FOR PARALELO REDUCTION NO NEGATIVE +
#pragma omp for reduction(+: negatives) reduction(minimo: pivot_row)
      for (int i = 0; i < lastRow; i++)
      {
        if(tableau[i * colSize + pivot_col.index] > 0)
        {
          double ratio = tableau[i * colSize + lastCol]/ tableau[i * colSize + pivot_col.index];

          if(ratio < pivot_row.val)
          {
            pivot_row.val = ratio;
            pivot_row.index = i;
          }

        } else
            negatives++;
      }
      // barreira 1
      // TODO ver se isso aqui não precisa ter a segunda barreira
// printf("ni = %d\t tid: %d \t  pivot_row -> val: %f \t index : %d \n" , ni,omp_get_thread_num(), pivot_row.val, pivot_row.index);

#pragma omp single nowait
{
      if (negatives == lastRow)
      {
        cout << "Solution not found\n";
        exit(EXIT_FAILURE);
      }
}

      // BARREIRA 2
      // GAUSSIAN REDUCTION USING BLOCKING

      // TODO deixar essa variável privada
      pivot = 1/tableau[pivot_row.index * colSize + pivot_col.index];

 //printf("ni = %d\t tid: %d \t  pivot -> val: %f \n" , ni, omp_get_thread_num(), pivot);

      // TODO block_init colocar isso privado
      for (int block_init = pivot_col.index + 1; block_init < colSize; block_init += B)
      {
        int lim = colSize  < block_init + B ? colSize : B + block_init;

        // NORMALIZING PIVOT ROW
        // FOR PARALELO
#pragma omp for
        for (int j = block_init; j < lim; j++) // vetorizado
        {
          tableau[pivot_row.index * colSize + j] *= pivot;
        }
        // BARREIRA IMPLICITA
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // FOR PARALELO COM NOWAIT
#pragma omp for nowait
        for (int current_row = 0; current_row < pivot_row.index; current_row++)
        {
          double tmp = tableau[current_row * colSize + pivot_col.index];
          for (int inner_block = block_init; inner_block < lim; inner_block++) // vetorizado
          {
            tableau [
                      ( current_row * colSize + (inner_block))
                    ]
                    -=
                          tableau [ pivot_row.index * colSize + (inner_block)] * tmp;
          }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // FOR PARALELO ( BARREIRA IMPLICITA)
#pragma omp for
        for (int current_row = pivot_row.index + 1; current_row < rowSize; current_row++)
        {
          double tmp = tableau[current_row * colSize + pivot_col.index];
          for (int inner_block = block_init; inner_block < lim; inner_block++) // vetorizado
          {
            tableau [
                      ( current_row * colSize + (inner_block))
                    ]
                    -=
                          tableau [ pivot_row.index * colSize + (inner_block)] * tmp;
          }
        }
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      }

      for (int block_init = 0; block_init <= pivot_col.index; block_init += B)
      {
        int lim = pivot_col.index + 1 < block_init + B ? pivot_col.index + 1 : B + block_init;

        // NORMALIZING PIVOT ROW
        // FOR PARALELO COM REDUCTION *
#pragma omp for
        for (int j = block_init; j < lim; j++) // vetorizado
        {
          tableau[pivot_row.index * colSize + j] *= pivot;
        }

        // BARREIRA
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // FOR PARALELO COM NOWAIT
#pragma omp for nowait
        for (int current_row = 0; current_row < pivot_row.index; current_row++)
        {
          double tmp = tableau[current_row * colSize + pivot_col.index];
          for (int inner_block = block_init; inner_block < lim; inner_block++) // vetorizado
          {
            tableau [
                      ( current_row * colSize + (inner_block))
                    ]
                    -=
                          tableau [ pivot_row.index * colSize + (inner_block)] * tmp;
          }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // FOR PARALELO
#pragma omp for
        for (int current_row = pivot_row.index + 1; current_row < rowSize; current_row++)
        {
          double tmp = tableau[current_row * colSize + pivot_col.index];
          for (int inner_block = block_init; inner_block < lim; inner_block++) // vetorizado
          {
            tableau [
                      ( current_row * colSize + (inner_block))
                    ]
                    -=
                          tableau [ pivot_row.index * colSize + (inner_block)] * tmp;
          }
        }
        // BARREIRA IMPLICITA
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      }
//printf("---------- FIM DE ITERACAO %d ------------\n", omp_get_thread_num());

    } while (true);
}
    if (clock_gettime(CLOCK_REALTIME, &timeTotalEnd)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    double ONE_SECOND_IN_NANOSECONDS = 1000000000;
    struct timespec time = My_diff(timeTotalInit, timeTotalEnd);
    double processTime = (time.tv_sec + (double) time.tv_nsec / ONE_SECOND_IN_NANOSECONDS);

    printf("%f %f ", processTime / ni, processTime);

    printf("%d %f \n", ni, tableau[lastRow * colSize + lastCol]);
    log_file.close();

  delete_matrix(tableau, rowSize + 1);
}
