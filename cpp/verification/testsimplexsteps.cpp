#include <iostream>
#include "functions.h"
#include "SimplexSteps.h"

using namespace std;

int main() {
    // Create a sample tableau with 4 rows and 6 columns
    double **tableau = new double*[4];
    for(int i = 0; i < 4; ++i) {
        tableau[i] = new double[6];
    }

    // // Initialize the tableau with your data
    // int nRow = 4, nCol = 6;
    // int count2 = 0;
    // char* filename = (char*)"data_3_2";
    // tableau = read_data(filename, nRow, nCol);

    // Initialize the tableau with your sample data
    tableau[0][0] = 1.0; tableau[0][1] = 0.0; tableau[0][2] = 1.0; tableau[0][3] = 0.0; tableau[0][4] = 0.0; tableau[0][5] = 4.0;
    tableau[1][0] = 0.0; tableau[1][1] = 2.0; tableau[1][2] = 0.0; tableau[1][3] = 1.0; tableau[1][4] = 0.0; tableau[1][5] = 12.0;
    tableau[2][0] = 3.0; tableau[2][1] = 2.0; tableau[2][2] = 0.0; tableau[2][3] = 0.0; tableau[2][4] = 1.0; tableau[2][5] = 18.0;
    tableau[3][0] = -3.0; tableau[3][1] = -5.0; tableau[3][2] = 0.0; tableau[3][3] = 0.0; tableau[3][4] = 0.0; tableau[3][5] = 0.0;

    int nRow = 4, nCol = 6;
    int count2 = 0;

    // Test findMaxObjective
    Compare_Max max = findMaxObjective(tableau, nRow-1, nCol-1);
    std::cout << "Max Objective Value: " << max.val << ", Column Index: " << max.index << std::endl;

    // Test findPivotRow
    Compare_Min min = findPivotRow(tableau, nRow-1, nCol-1, max.index, count2);
    std::cout << "Min Pivot Row Value: " << min.val << ", Row Index: " << min.index << std::endl;
    std::cout << "count2: " << count2 << endl;

    // Test updateRow
    updateRow(tableau, nRow-1, nCol-1, min.index, max.index);
    std::cout << "Updated Row: ";
    for(int j = 0; j < nCol; ++j) {
        std::cout << tableau[min.index][j] << " ";
    }
    std::cout << std::endl;

    // Test performRowOperations
    performRowOperations(tableau, nRow-1, nCol-1, min.index, max.index);
    std::cout << "Tableau after row operations:" << std::endl;
    for(int i = 0; i < nRow; ++i) {
        for(int j = 0; j < nCol; ++j) {
            std::cout << tableau[i][j] << " ";
        }
        std::cout << std::endl;
    }

    // Test updateObjective

    Compare_Max updatedmax = updateObjective(tableau, nRow-1, nCol-1, min.index, max.index, count2);
    std::cout << "Updated Max Objective Value: " << updatedmax.val << ", Column Index: " << updatedmax.index << std::endl;
    std::cout << "count2: " << count2 << endl;

    // Clean up
    for(int i = 0; i < nRow; ++i) {
        delete[] tableau[i];
    }
    delete[] tableau;

    return 0;
}
