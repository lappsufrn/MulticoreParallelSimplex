#include "../include/util.h"  // Include the util.h header file
#include <cassert> // For assert function

// Function to print the tableau matrix
void print_tableau() {
    for (int i = 0; i < dimension[0]; ++i) {
        for (int j = 0; j < dimension[1]; ++j) {
            std::cout << tableau[i][j] << " ";
        }
        std::cout << std::endl;
    }
}

int main(int argc, char** argv) {
    // Test allocate_array function
    int size = 5;
    int* test_array = allocate_array<int>(size);
    assert(test_array != nullptr);

    // Test allocate_matrix function
    dimension = new int[2];
    dimension[0] = 3; // Number of rows
    dimension[1] = 4; // Number of columns
    allocate_matrix();
    assert(tableau != nullptr);

    // Test delete_all function
    delete_all();
    std::cout << "Value of tableau: " << tableau << std::endl;
    assert(tableau == NULL);

    // Test read_data function
    // Create a test file named "test_data.txt" with appropriate content
    char* file_args[] = {(char*)"./a.out", (char*)"util_test_data"};
    read_data(file_args);
    print_tableau(); // This should print the tableau matrix

    // Clean-up
    delete_all();

    std::cout << "All tests passed!" << std::endl;

    return 0;
}
