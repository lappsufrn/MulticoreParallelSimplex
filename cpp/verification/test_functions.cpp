#include <iostream>
#include "functions.h"  // Include the functions header file

using namespace std;

int main(int argc, char** argv) {
    // Test the get_dimension function
    int nRow, nCol;
    char* filename = (char*)"data_3_2";  // Replace with your actual filename
    cout << "filename: " << filename << " and ";
    get_dimension(&filename, nRow, nCol);
    cout << "Dimensions: " << nRow << "x" << nCol << endl;

    // Test the read_data function
    double **tableau = read_data(&filename, nRow, nCol);

    // Test the printRow and printCol functions
    cout << "Printing first row:" << endl;
    printRow(0, nCol - 1, tableau);

    cout << "Printing first column:" << endl;
    printCol(0, nRow - 1, tableau);

    // Test the allocate_matrix function
    double **new_matrix = allocate_matrix(3, 3);
    cout << "Allocated new 3x3 matrix." << endl;

    // Test the delete_matrix function
    delete_matrix(new_matrix, 3);
    cout << "Deallocated the 3x3 matrix." << endl;

    // Test the My_diff function
    timespec start, end, diff;
    start.tv_sec = 1;
    start.tv_nsec = 500;
    end.tv_sec = 2;
    end.tv_nsec = 1000;
    diff = My_diff(start, end);
    cout << "Time difference: " << diff.tv_sec << "s and " << diff.tv_nsec << "ns" << endl;

    // Deallocate the tableau matrix
    delete_matrix(tableau, nRow);

    return 0;
}
