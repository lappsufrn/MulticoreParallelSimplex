#include <iostream>
#include "newfunctions.h" // Include the header file containing the functions to be tested

using namespace std;

int main() {
    // Initialize variables for number of rows and columns
    int nRow, nCol;

    // Define the filename
    char filename[] = "data_3_2"; // Make sure this file exists in the same directory or provide the full path
  

    // Test get_dimension function
    get_dimension(filename, nRow, nCol);
    cout << "filename: " << filename << "\n";
    cout << "Dimensions: " << nRow << "x" << nCol << "\n" << endl;

    // Test read_data function
    double *tableau = read_data(filename, nRow, nCol);
     cout << "Dimensions: " << nRow << "x" << nCol << "\n" << endl;

    // Test the printRow and printCol functions
    cout << "Printing first row:" << endl;
    cout << "From cout" << " with " << nCol << " columns " << endl;
    for (int col = 0; col < nCol; ++col) {
        cout << col << ": " << tableau[0 *nCol + col] << endl;
    }
    cout << "From printRow function" << endl;
    printRow(0, nCol - 1, tableau);

    cout << "Printing first column:" << endl;
    cout << "From cout" << " with " << nRow << "rows " << endl;
    for (int row = 0; row < nRow; ++row) {
        cout << row << ": " << tableau[row * nCol + 0] << endl;
    }
    cout << "From printCol function" << endl;
    printCol(0, nRow - 1, nCol, tableau);

    // Test the allocate_matrix function
    double *new_matrix = allocate_matrix(3, 3);
    cout << "Allocated new 3x3 matrix." << endl;

    // Test the delete_matrix function
    delete_matrix(new_matrix, 3);
    cout << "Deallocated the 3x3 matrix." << endl;

    // Test My_diff function
    timespec start, end, result;
    start.tv_sec = 1;
    start.tv_nsec = 100;
    end.tv_sec = 2;
    end.tv_nsec = 200;
    result = My_diff(start, end);
    cout << "Time difference: " << result.tv_sec << "s " << result.tv_nsec << "ns" << endl;

    // Test string conversion functions
    string testString = "1.23 4.56 7.89";
    vector<double> testVector = string_to_vector<double>(testString);
    for (double val : testVector) {
        cout << val << " ";
    }
    cout << endl;

    // Deallocate the tableau matrix
    delete_matrix(tableau, nRow);
    cout << "Deallocated the tableau matrix." << endl;

    return 0;
}
