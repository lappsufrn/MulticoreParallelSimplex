// ----------------------------------------------------------------------------
/**
 * @file  util.h
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    04/2017
 *
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 * 
 * @brief 
 * 
 * @language: C++
 *
 * @section Description
 * The util.h file is a utility header file that provides various functions for array and matrix allocation, file reading, and data conversion. 
 * It also declares some global variables to be used in the simplex algorithm.
 * 
 * @subsection 
 * 
 */
// ----------------------------------------------------------------------------

// The header guard #ifndef UTIL_H ensures that the header file is included only once during compilation, preventing duplicate declarations.
#ifndef UTIL_H
#define UTIL_H

// Include necessary C++ standard libraries required for the program.
#include <iostream>  //Standard Input-Output Stream Library. Provides functionalities for standard input and output.
#include <string>    //C++ Standard String Library. Provides the string class for C++.
#include <algorithm> //Powerful tool for data manipulation in C++, with a set of functions that can be used to perform common operations.
#include <vector>    //Standard Template Library (STL) Vector. Provides dynamic array functionalities.
#include <sstream>   //String Stream Library. Provides string stream classes for parsing strings.
#include <fstream>   //File Stream Library. Provides functionalities for file input and output.
#include <math.h>    //C Standard Math Library. Provides mathematical functions

// Include the file debug.h
#include "debug.h"

// Directive to compiler to use classes and functions without specifying they are in namespace std 
using namespace std;

// Global variables for the simplex algorithm

/*
 * Global variables are declared here to store the tableau and other parameters for the simplex algorithm
 */
long double **tableau = NULL;
int * index_constraint_artificial = NULL;
int * index_variable_artificial = NULL;
int * dimension = NULL;
int * foa = NULL;
int index_va_size;
const long double LIMIT = pow(10,-2);
/*---------*/

// Utility functions for memory allocation

/*
 * These functions handle dynamic memory allocation for arrays and the tableau matrix. 
 * They use new to allocate memory and check for allocation failure.
 * There is also a function to deallocate all the dynamically allocated memory to prevent memory leaks.
 */

/**
 * Template function to allocate an array of type T and given size
 */
template <class T>
T * allocate_array(int size) {
    T * array = new (nothrow) T[size];

    if (array == 0) {
        cerr << "It was not possible to allocate array \n";
        exit(EXIT_FAILURE);
    }

    return array;
}

/**
 * Function to allocate the tableau matrix
 */
void allocate_matrix() {

    int nL = dimension[0];
    int nC = dimension[1];

    tableau = new (nothrow) long double *[nL];

    if (tableau == 0) {
        cerr << "It was not possible to allocate matrix \n";
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < nL; i++) {
        *(tableau + i) = new (nothrow) long double[nC];
        if ((tableau + i) == 0) {
            cerr << "It was not possible to allocate matrix \n";
            exit(EXIT_FAILURE);
        }
    }

}

/*
 * Function to deallocate memory
 */
void delete_all() {
    if (tableau == 0) return;
    //if (tableau == NULL) return;

    for (int i = 0; i < dimension[0]; i++) {
        delete [] tableau[i];
    }

    delete [] tableau;
    tableau = NULL;  // Set tableau to NULL

    delete [] index_constraint_artificial;
    index_constraint_artificial = NULL; 

    delete [] index_variable_artificial;
    index_variable_artificial = NULL;

    delete [] dimension;
    dimension = NULL;

    delete [] foa;
    foa = NULL;
}


// String Conversion Functions

/*
 * These template functions convert strings to numerical types. 
 * They are used for parsing input files into the data structures used by the simplex algorithm.
 */

/*
 * Utility function to convert string to double
 */
template <class T>
bool from_string(T& t, const string& s, ios_base& (*f)(ios_base&)) {
    istringstream iss(s);
    return !(iss >> f >> t).fail();
}

/**
 * Utility function to convert coefficients of a line from string to double
 */
template <class T>
vector<T> string_to_vector(string s) {
    istringstream iss(s);
    vector<T> vec;
    do {
        string sub;
        iss >> sub;
        T numb;
        if (from_string<T>(numb, sub, dec)) {
            vec.push_back(numb);
        }
    } while (iss);
    return vec;
}

/**
 * Utility function to retrieve a line from a file
 */
string get_line(ifstream& file) {

    char buffer[1000000];
    file.getline(buffer, 1000000);

    string s(buffer);

    return s;
}

/**
 * Utility function to fill in arrays
 */
template <class T>
int fill_array(ifstream& file, T ** array) {
    string s = get_line(file);
    vector<T> vec = string_to_vector<T>(s);
    *array = allocate_array<T>(vec.size());

    copy(vec.begin(), vec.end(), *array);

    return vec.size();
}

/**
 * Utility function to read from file the necessary information for simplex algorithm
 * 
 * data file should have no extension. First line should include two int for number of rows and number of columns. End file with empty newline
 */
void read_data(char** argv) {

    ifstream file(argv[1]);

    if (!file.is_open()) {
        cerr << "Error opening file";
        exit(1);
    }
    
    // fill_array<int>(file, &index_constraint_artificial);
    // index_va_size = fill_array<int>(file, &index_variable_artificial);
    // fill_array<int>(file, &foa);
    fill_array<int>(file, &dimension);
    
    allocate_matrix();

    int lin = 0;
    while (!file.eof()) {
        string s = get_line(file);

        if (s == "") break;

        vector<long double> contraint = string_to_vector<long double>(s);
        copy(contraint.begin(), contraint.end(), tableau[lin]);
        lin++;
    }

    debug(tableau, index_constraint_artificial, index_variable_artificial, dimension,index_va_size);
    debug(dimension[0],dimension[1]);
    debug("-----------\n");
}

#endif /* UTIL_H */

