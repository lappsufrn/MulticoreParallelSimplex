// ----------------------------------------------------------------------------
/**
 * @file  simplex.h
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    04/2017
 *
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 * 
 * @brief 
 * 
 * @language: C++
 *
 * @section Description
 * The header file simplex.h is designed to implement the Simplex algorithm for solving linear programming problems. 
 * It includes functions for operations on rows, finding the pivot element, and executing the Simplex algorithm itself.
 * 
 * @subsection 
 * 
 */
// ----------------------------------------------------------------------------

// The header guard #ifndef SIMPLEX_H ensures that the header file is included only once during compilation, preventing duplicate declarations.
#ifndef SIMPLEX_H
#define SIMPLEX_H

// Include necessary C++ standard libraries required for the program.
#include <cmath>
#include "time.h"

// Include files debug.h and util.h
#include "debug.h"
#include "util.h"

/*
 * Function to perform row operations for zeroing out elements below the pivot element.
 * Mathematical Basis: newRow_i = oldRow_i - (oldRow_i[pivot_column] / oldRow_pivot_row[pivot_column]) * oldRow_pivot_row
 * @param pivot_row: The row containing the pivot element.
 * @param pivot_column: The column containing the pivot element.
 * @param row_max: The total number of rows.
 * @param col_max: The total number of columns.
 */

void operation_lines(int pivot_row, int pivot_column, int row_max, int col_max) {
    debug("OPERATION");
    for (int i = 0; i < row_max; i++) {
        if (i == pivot_row)
            continue;
        else {

            //            if (dimension[0] - 1 == i) {
            //                cout << "i " << i << endl;
            //                cout << "pivot_column " << pivot_column << endl;
            //                cout << "pivot = -tableau[i][pivot_column] " << pivot << endl;
            //            }
            long double pivot = -tableau[i][pivot_column];
            for (int k = 0; k < col_max; k++) {
                long double temp = (pivot * tableau[pivot_row][k]);
                tableau[i][k] = temp + tableau[i][k];

                //                if (dimension[0] - 1 == i) {
                //                    cout << "k " << k << endl;
                //                    cout << "pivot_row " << pivot_row << endl;
                //                    cout << "tableau[pivot_row][k] " << tableau[pivot_row][k] << endl;
                //                    cout << "temp = (pivot * tableau[pivot_row][k]) "
                //                            << temp << endl;
                //                    cout << "tableau[i][k] " << tableau[i][k] << endl;
                //                }



                //                if (dimension[0] - 1 == i) {
                //                        cout << "tableau[i][k] = temp + tableau[i][k] " << tableau[i][k] << endl;
                //                }
            }
        }
    }
}

/*
 * Function performs the Simplex algorithm to maximize the objective function.
 */
void simplex(long double &optValue, int & ni) {


    ni = 0;

    int i, j, k, pivot_column, pivot_row, count;
    long double maximo, menor, pivot, temp;

    int const ROWS_MAX = dimension[0];
    int const COL_MAX = dimension[1];

    //    debug<int>("ROWS_MAX ", ROWS_MAX);
    //    debug<int>("COL_MAX ", COL_MAX);

    // debug_track("Quadro");
    //  display_matrix(tableau, ROWS_MAX, COL_MAX);
    debug("***ITERAÇÃO *** ", ni);

    debug("FOA OU FO");
    //display_matrix(tableau, ROWS_MAX, COL_MAX, ROWS_MAX - 1, 1);
    do {

        //Verificar qual variável deve entrar na base
        pivot_column = -1;
        maximo = -HUGE_VALL;
        for (j = 0; j < COL_MAX - 1; j++) {
            if (tableau[ROWS_MAX - 1][j] < 0) {
                //cout << "j " << j << " " << tableau[ROWS_MAX - 1][j] << endl;
                temp = -tableau[ROWS_MAX - 1][j];
                if (maximo <= temp) {
                    if (menor == temp && drand48() > .5) {
                        maximo = temp;
                        pivot_column = j;
                    } else {
                        maximo = temp;
                        pivot_column = j;
                    }
                }
            }
        }
        //Sair caso o primeiro quadro ja for ótimo.
        if (maximo == -HUGE_VALL) break;
        //atualizar lista de variáveis básicas


        debug<int>("pivot column ", pivot_column);

        count = 0;
        pivot = HUGE_VALL;
        menor = HUGE_VALL;
        pivot_row = -1;
        //Verificar qual variável da base deve sair, conferindo o menor cociente

        long double temp;
        for (i = 0; i < ROWS_MAX - 1; i++) {
            //cout << "i " << i << " tableau[i][pivot_column] " << tableau[i][pivot_column] << endl;
            if (tableau[i][pivot_column] > 0) {
                temp = tableau[i][COL_MAX - 1] / tableau[i][pivot_column];

                if (menor >= temp) {
                    if (menor == temp && drand48() > .3) {
                        menor = temp;
                        pivot_row = i;
                    } else {
                        menor = temp;
                        pivot_row = i;
                    }
                }
            } else {
                count++;
            }

        }

        if (count == ROWS_MAX - 1) {
            cerr << "Solucao nao encontrada\n";
            exit(EXIT_FAILURE);
        }

        debug<int>("pivot_row ", pivot_row);


        cout << "pivot_row " << pivot_row << "\n" << tableau[pivot_row][COL_MAX - 1] << "/" << tableau[pivot_row][pivot_column]
                << " = " << tableau[pivot_row][COL_MAX - 1] / tableau[pivot_row][pivot_column] << endl;
        //Divide toda a linha pelo pivo.
        pivot = tableau[pivot_row][pivot_column];

        debug<int>("pivot ", pivot);


        //        debug("Linha do pivo antes");
        //        display_matrix(tableau, ROWS_MAX, COL_MAX, pivot_row, 1);

        for (j = 0; j < COL_MAX; j++)
            tableau[pivot_row][j] = tableau[pivot_row][j] / pivot;

        //        debug("Linha do pivo depois");
        //        display_matrix(tableau, ROWS_MAX, COL_MAX, pivot_row, 1);

        //        debug("fo antes");
        //        display_matrix(tableau, ROWS_MAX, COL_MAX, ROWS_MAX - 1, 1);

        //Zerar a coluna do pivô para que apareça a identidade
        operation_lines(pivot_row, pivot_column, ROWS_MAX, COL_MAX);

        debug("fo depois");
        display_matrix(tableau, ROWS_MAX, COL_MAX, ROWS_MAX - 1, 1);


        count = 0;

        for (j = 0; j < COL_MAX - 1; j++)
            if (tableau[ROWS_MAX - 1][j] >= 0)
                count++;

        ni++;
        //        debug_track("Quadro");
        //        display_matrix(tableau, ROWS_MAX, COL_MAX);
        debug("***ITERAÇÃO *** ", ni);
        debug("FOA OU FO");
        cout << "opt " << tableau[ROWS_MAX - 1][COL_MAX - 1] << endl;
        //display_matrix(tableau, ROWS_MAX, COL_MAX, ROWS_MAX - 1, 1);
        //debug<int>("Count ", count);
        //debug<int>("COL_MAX - 1 ", COL_MAX - 1);

    } while (count != (COL_MAX - 1));

    optValue = tableau[ROWS_MAX - 1][COL_MAX - 1];

}
//
///**
// * Função para iniciar a segunda fase.
// * @param constraint
// * @param variable
// */
//void start_fase_2(long double &optValue, int& ni) {
//    debug("----");
//    debug("Start fase 2");
//    debug("----");
//    //    for (int i = 0; i < dimension[1]; i++) {
//    //        //Multiplicando por -1 a função objetivo
//    //        //Penultima linha é a função objetivo original
//    //        tableau[dimension[0] - 2][i] = -tableau[dimension[0] - 2][i];
//    //    }
//    dimension[0]--;
//    simplex(optValue, ni, false);
//}
//
///**
// * Função 
// */
//int remove_one_equation(const int row_max, const int col_max, int i) {
//    for (int linha = i; linha < (row_max - 1); linha++) {
//        for (int coluna = 0; coluna < col_max; coluna++) {
//            tableau[linha][coluna] = tableau[linha + 1][coluna];
//        }
//    }
//    return row_max - 1;
//}
//
///**
// * Função 
// */
//int remove_one_column(const int row_max, const int col_max, int index) {
//    for (int coluna = index; coluna < col_max - 1; coluna++) {
//        for (int linha = 0; linha < row_max; linha++) {
//            tableau[linha][coluna] = tableau[linha][coluna + 1];
//        }
//    }
//    return col_max - 1;
//}
//
//bool nonzero_variable_original(int & k, int i, bool basic_variables[]) {
//    bool check_vari_origin_nzero = false;
//
//    for (k = 0; k < dimension[1] - 1; k++) {
//        /*O elemento k da linha referente a v.a. básica (index_variable_va[i])
//         * é diferente de zero && não é básica && não é v.a?
//         */
//        if (!(!(abs(tableau[index_constraint_artificial[i]][k]) != 0) || basic_variables[k] ||
//                foa[k])) {
//            debug("quem é a variavel k ", k);
//            debug("valor ", tableau[index_constraint_artificial[i]][k]);
//            check_vari_origin_nzero = true;
//            break;
//        }
//    }
//    return check_vari_origin_nzero;
//}
//
///**
// * Algoritmo duas fases
// * @param constraint
// * @param variable
// * @param optValue
// * @return 
// */
//void two_fases(long double &optValue, int& ni) {
//
//    /* Tableau =
//     * Penultima linha é a função objetivo original
//     * Ultima linha é a função objetivo artificial  
//     */
//
//    //verificando se não há variáveis artificais.
//    if (index_va_size == 0) {
//        start_fase_2(optValue, ni);
//        return;
//    }
//    //------
//
//    /**----
//     * Iniciar fase 1
//     * ----
//     */
//    simplex(optValue, ni, true);
//
//    debug<long double>("Fase 1 Completed - ", optValue);
//    //    cout <<"Fase 1 Completed - " << optValue;
//
//    if (optValue != 0 && abs(optValue) > LIMIT) {
//        cerr << "Solucao inviável\n";
//        exit(1);
//    }
//
//    bool basic_variables[dimension[1] - 1];
//
//    //verificar quais colunas são básicas
//    long double sum;
//    for (int j = 0; j < dimension[1] - 1; j++) {
//        sum = 0;
//        for (int i = 0; i < dimension[0]; i++)
//            sum += tableau[i][j];
//
//        if (sum == 1) {
//            //V BASICA
//            basic_variables[j] = 1;
//        } else {
//            basic_variables[j] = 0;
//        }
//    }
//
//    debug("basic_va array:");
//    print_array<bool>(basic_variables, dimension[1] - 1);
//    debug("**Etapa de identificação de variáveis artificiais básicas");
//    //Etapa de identificação de variáveis artificiais básicas
//    for (int i = index_va_size - 1; i >= 0; i--) {
//        debug<int>("i ", i);
//        debug<int>("index_variable_va[i]", index_variable_artificial[i]);
//        debug<int>("basic_va ", basic_variables[index_variable_artificial[i]]);
//
//        if (basic_variables[index_variable_artificial[i]]) {
//            //V.A. BÁSICA        
//            int k;
//            bool result = nonzero_variable_original(k, i, basic_variables);
//
//            if (result) {
//                //pivotear
//                for (int j = 0; j < dimension[1]; j++)
//                    tableau[index_constraint_artificial[i]][j] /= tableau[index_constraint_artificial[i]][k];
//
//                operation_lines(index_constraint_artificial[i], k, dimension[0], dimension[1]);
//            } else {
//                debug("***EXCLUIR LINHA***");
//                debug("index_constraint_va[i]", index_constraint_artificial[i]);
//                //excluir linha da respectiva variavel artificial
//                dimension[0] = remove_one_equation(dimension[0], dimension[1], index_constraint_artificial[i]);
//            }
//        }
//        dimension[1] = remove_one_column(dimension[0], dimension[1], index_variable_artificial[i]);
//    }
//
//    start_fase_2(optValue, ni);
//}
#endif /* SIMPLEX_H */

