// ----------------------------------------------------------------------------
/**
 * @file  functions.h
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    04/2017
 *
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 * 
 * @brief 
 * 
 * @language: C++
 *
 * @section Description
 * 
 * 
 * @subsection 
 * 
 */
// ----------------------------------------------------------------------------

// The header guard #ifndef FUNCTIONS_H ensures that the header file is included only once during compilation, preventing duplicate declarations.
#ifndef FUNCTIONS_H
#define FUNCTIONS_H


// Include necessary C++ standard libraries required for the program.
#include <iostream>  //Standard Input-Output Stream Library. Provides functionalities for standard input and output.
#include <string>    //C++ Standard String Library. Provides the string class for C++.
#include <vector>    //Standard Template Library (STL) Vector. Provides dynamic array functionalities.
#include <sstream>   //String Stream Library. Provides string stream classes for parsing strings.
#include <fstream>   //File Stream Library. Provides functionalities for file input and output.
#include <cstring>   //Standard C++ library collection that provides commonly used methods for C-style string manipulation
#include <stdlib.h>  //Standard library. Provides general-purpose functions, including dynamic memory management, random numbers, and process control.
#include <cstdio>    //C Standard Input and Output. Provides functions for file and console I/O operations.
#include <cmath>     //C Mathematical Functions. Provides mathematical functions and constants.
#include <cstdlib>   //C Standard General Utilities Library. Essentially the same as <stdlib.h> but in a C++ context.
#include "time.h"    //Time Functions. Provides functions to get and manipulate time.


// Directive to compiler to use classes and functions without specifying they are in namespace std 
using namespace std;

// MOVED TO MAIN
// // Declare a variable named log_file of the type ofstream (Output File Stream).
// ofstream log_file;

/*
 * Function to calculate difference between two timespec parameters end - start
 */
struct timespec My_diff(struct timespec start, struct timespec end) {
    struct timespec temp;
    // If the difference in nanoseconds is negative, then one second should be added
    // to the nanoseconds field and borrow one second from the seconds field.
    // It's like a subtraction where you borrow '1' from the next most significant number.
    if ((end.tv_nsec - start.tv_nsec) < 0) {
        temp.tv_sec = end.tv_sec - start.tv_sec - 1;
        temp.tv_nsec = 1000000000 + end.tv_nsec - start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec - start.tv_sec;
        temp.tv_nsec = end.tv_nsec - start.tv_nsec;
    }
    return temp;
}


// Utility functions for memory allocation

/*
 * These functions handle dynamic memory allocation for the tableau matrix. 
 * They use new to allocate memory and check for allocation failure.
 * There is also a function to deallocate all the dynamically allocated memory to prevent memory leaks.
 */

/*
 * Function to allocate the tableau matrix
 */
double ** allocate_matrix(int nRow, int nCol) {

    double **tableau = new (nothrow) double *[nRow];

    if (tableau == 0) {
        cerr << "It was not possible to allocate matrix \n";
        exit(EXIT_FAILURE);
    }

    for (int i = 0; i < nRow; i++) {
        *(tableau + i) = new (nothrow) double[nCol];
        if ((tableau + i) == 0) {
            cerr << "It was not possible to allocate matrix \n";
            exit(EXIT_FAILURE);
        }
    }
    return tableau;
}

/*
 * Function to deallocate memory
 */
void delete_matrix(double **tableau, int nRow) {

    if (tableau == 0) return;

    for (int i = 0; i < nRow; i++) {

        delete [] tableau[i];
    }

    delete [] tableau;
    tableau = NULL;  // Set tableau to NULL
}


// String Conversion Functions

/*
 * These template functions convert strings to numerical types. 
 * They are used for parsing input files into the data structures used by the simplex algorithm.
 */

/*
 * Utility function to convert string to double
 */
template <class T>
bool from_string(T& t, const string& s, ios_base& (*f)(ios_base&)) {
    istringstream iss(s);
    return !(iss >> f >> t).fail();
}


/**
 * Utility function to convert coefficients of a line from string to double
 */
template <class T>
vector<T> string_to_vector(string s) {
    istringstream iss(s);
    vector<T> vec;
    do {
        string sub;
        iss >> sub;
        T numb;
        if (from_string<T>(numb, sub, dec)) {
            vec.push_back(numb);
        }
    } while (iss);
    return vec;
}

/*
 * Utility function to read from file the necessary information for simplex algorithm
 * 
 * data file should have no extension. End file with empty newline. Name the file 'data_NumConstraints_NumVariables'
 */


/*
 * When running from command-line, use argv

void get_dimension(char** argv, int &nRow, int &nCol) {
    int dimension[2]={0,0}, i = 0;
    char *pch = strtok(argv[1], "x/_");
    while (pch != NULL) {
        
        if (i == 1) {
            dimension[0] = atoi(pch);
        }
        if (i == 2) {
            dimension[1] = atoi(pch);
        }
        pch = strtok(NULL, "x/_");
        i++;
    }
    nRow = dimension[0] + 1;
    nCol = dimension[0]+ dimension[1]+1;
}


double ** read_data(char** argv, int& nRow, int& nCol) {

    ifstream file(argv[1]);
  
    log_file.open("./logs/log_cpp",ofstream::app);

    if (!file.is_open()) {
        cerr << "Error opening file";
        exit(1);
    }

    get_dimension(argv, nRow, nCol);

    log_file << "----" << nRow << "x" << nCol <<"----"<< endl << endl;

    double ** tableau = allocate_matrix(nRow, nCol);
  
    int lin = 0;
    string line;
    vector<int> sizes_col;
    while (getline (file,line)) {
        
        if (line == "") break;

        vector<double> contraint = string_to_vector<double>(line);
        copy(contraint.begin(), contraint.end(), tableau[lin]);
        lin++;
        sizes_col.push_back(contraint.size());
    }
*/

// When calling functions from code, it should pass filename instead of argv

void get_dimension(const char* filename, int &nRow, int &nCol) {
    // strtok was changing filename inside read_data
    char temp_filename[50];  // Assuming filename will not exceed 50 characters
    strcpy(temp_filename, filename);  // Copy the original filename to temp_filename
    int dimension[2]={0,0}, i = 0;
    char *pch = strtok(temp_filename, "x/_");
    while (pch != NULL) {
        
        if (i == 1) {
            dimension[0] = atoi(pch);
        }
        if (i == 2) {
            dimension[1] = atoi(pch);
        }
        pch = strtok(NULL, "x/_");
        i++;
    }
    nRow = dimension[0] + 1;
    nCol = dimension[0]+ dimension[1]+1;
}


double ** read_data(char* filename, int& nRow, int& nCol) {

    ifstream file(filename);

    // MOVED TO MAIN
    // log_file.open("./logs/log_cpp",ofstream::app);

    if (!file.is_open()) {
        cerr << "Error opening file";
        exit(1);
    }

    get_dimension(filename, nRow, nCol);

    // MOVED TO MAIN
    // log_file << "----" << nRow << "x" << nCol <<"----"<< endl << endl;

    double ** tableau = allocate_matrix(nRow, nCol);
  
    int lin = 0;
    string line;
    vector<int> sizes_col;
    while (getline (file,line)) {
        
        if (line == "") break;

        vector<double> contraint = string_to_vector<double>(line);
        copy(contraint.begin(), contraint.end(), tableau[lin]);
        lin++;
        sizes_col.push_back(contraint.size());
    }

   // MOVED TO MAIN
//    //Create log file
       
//     log_file << "lin " << lin << endl << "col:\n";

//     for (uint i = 0; i < sizes_col.size(); i++)
//     {
//         log_file << i << " : " << sizes_col[i] << endl;
//     }
       
//     log_file.close();

    return tableau;
}

void printRow(int i, int colMax, double **tableau) {
    int col;
    for (col = 0; col <= colMax; col++) {
        printf("%d: %f ", col, tableau[i][col]);
    }
    printf("\n");
}

void printCol(int c, int rowMax, double **tableau) {
    int lin;

    for (lin = 0; lin <= rowMax; lin++) {
        printf("%d: \t %f \n", lin, tableau[lin][c]);
    }
}

void printMatrix(double** tableau, int nRow, int nCol)
{
  for (int i = 0 ; i < nRow + 1; i++)
  {
    for (int j = 0; j < nCol + 1; j++)
    {
      cout << tableau[i][j] << " ";
    }
    cout << endl;
  }
}

#endif /* FUNCTIONS_H */

