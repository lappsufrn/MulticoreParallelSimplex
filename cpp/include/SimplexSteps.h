// ----------------------------------------------------------------------------
/**
 * @file  SimplexSteps.h
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 *
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 * 
 * @brief 
 * 
 * @language: C++
 *
 * @section Description
 * 
 * 
 * @subsection 
 * 
 */
// ----------------------------------------------------------------------------

#ifndef SIMPLEXSTEPS_H
#define SIMPLEXSTEPS_H

#include <vector>
#include <cmath>
#include <stdexcept>
#include <omp.h>
#include "openmp_utils.h"  // Include the OpenMP utilities header for custom reduction

using namespace std;

// STEP 2
// Function to find the maximum objective value in the last row of the tableau
Compare_Max findMaxObjective(double **tableau, int nRow, int nCol) {
    Compare_Max max;
    #pragma omp parallel for reduction(maximo:max)
    for (int j = 0; j <= nCol; j++) {
        if (tableau[nRow][j] < 0.0 && max.val < (-tableau[nRow][j])) {
            max.val = -tableau[nRow][j];
            max.index = j;
        }
    }

    // For debugging
    cout << "max.index: " << max.index << endl;

    return max;
}

// STEP 3
// Function to find the pivot row based on the maximum objective column
Compare_Min findPivotRow(double **tableau, int nRow, int nCol, int maxIndex, int *count2) {
    Compare_Min min;
    double pivot;
    int count = 0;
    #pragma omp parallel for reduction(+:count),reduction(minimo:min)
        for (int i = 0; i < nRow; i++) {
            if (tableau[i][maxIndex] > 0.0) {
                pivot = tableau[i][nCol] / tableau[i][maxIndex];
                if (min.val > pivot) {
                    min.val = pivot;
                    min.index = i;
                }
            } else {
                count++;
            }
        }
  
    // For debugging
    cout << "min.index: " << min.index << endl;
    cout << "count: " << count << endl;

    #pragma omp single nowait
    {
        if (count == nRow) {
            throw std::runtime_error("Solution not found");
        } else{
            count = 0;
            *count2 = 0;
        }
    }

    cout << "count: " << count << endl;
    cout << "count2: " << *count2 << endl;

    return min;
}

// STEP 4
// Function to update a row based on the pivot element
void updateRow(double **tableau, int nRow, int nCol, int pivotRow, int pivotColumn) {
    double pivot = tableau[pivotRow][pivotColumn];
    cout << "pivot: " << pivot << endl;
    #pragma omp parallel for
    for (int j = 0; j <= nCol; j++) {
        tableau[pivotRow][j] /= pivot;
    }
}


// STEP 5
// Function to perform row operations based on the pivot element and pivot column
void performRowOperations(double **tableau, int nRow, int nCol, int pivotRow, int pivotCol) {
    double pivot2;
    int i, j;
    #pragma omp parallel for private(pivot2, i, j)
    for (i = 0; i <= nRow; i++) {
        if (i != pivotRow) {
            pivot2 = -tableau[i][pivotCol];
            cout << "i: " << i << " pivot2: " << pivot2 << endl;
            #pragma GCC ivdep
            for (j = 0; j <= nCol; j++) {
                tableau[i][j] = (pivot2 * tableau[pivotRow][j]) + tableau[i][j];
            }
        }
    }
}

// STEP 6
// Function to update the objective row
Compare_Max updateObjective(double **tableau, int nRow, int nCol, int pivotRow, int pivotColumn, int *count2) {
    Compare_Max max;
    double pivot3 = -tableau[nRow][pivotColumn];
    cout << "pivot3: " << pivot3 << endl;
    #pragma omp parallel for
    for (int j = 0; j <= nCol; j++) {
        tableau[nRow][j] = (pivot3 * tableau[pivotRow][j]) + tableau[nRow][j];
        if (j < nCol && tableau[nRow][j] < 0.0) {
            #pragma omp atomic
            (*count2)++;
            if (max.val < (-tableau[nRow][j])) {
                max.val = -tableau[nRow][j];
                max.index = j;
            }
        }
    }

    // For debugging
    cout << "max.index: " << max.index << endl;
    cout << "count2: " << *count2 << endl;

    return max;
}

#endif // SIMPLEXSTEPS_H
