// ----------------------------------------------------------------------------
/**
 * @file  openmp_utils.h
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 *
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 * 
 * @brief 
 * 
 * @language: C++
 *
 * @section Description
 * 
 * 
 * @subsection 
 * 
 */
// ----------------------------------------------------------------------------

#ifndef OPENMP_UTILS_H
#define OPENMP_UTILS_H

#include <stdlib.h>
#include <cstdio>
#include <sstream>
#include <iostream>
#include <fstream>
#include <omp.h>

struct Compare_Max {
    double val = 0;
    int index = -1;
};

struct Compare_Min {
    double val = HUGE_VAL;
    int index = -1;
};

#pragma omp declare reduction(minimo : Compare_Min : omp_out = omp_in.val < omp_out.val ? omp_in : omp_out)
#pragma omp declare reduction(maximo : Compare_Max : omp_out = omp_in.val > omp_out.val ? omp_in : omp_out)

#endif // OPENMP_UTILS_H
