// ----------------------------------------------------------------------------
/**
 * @file  debug.h
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    04/2017
 *
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 * 
 * @brief 
 * 
 * @language: C++
 *
 * @section Description
 * The debug.h file is a utility header file that provides various functions for time complexity
 * and performance metrics, debugging and logging, and error handling
 * 
 * @subsection 
 * 
 */
// ----------------------------------------------------------------------------

// The header guard #ifndef DEBUG_H ensures that the header file is included only once during compilation, preventing duplicate declarations.
#ifndef DEBUG_H
#define DEBUG_H

// Include necessary C++ standard libraries required for the program.
#include <iostream>   //Standard Input-Output Stream Library. Provides functionalities for standard input and output.
#include <string>     //C++ Standard String Library. Provides the string class for C++.
#include <cstring>    //Standard C++ library collection that provides commonly used methods for C-style string manipulation

// Directive to compiler to use classes and functions without specifying they are in namespace std 
using namespace std;

/*
 * Global Variables and Constants
 */
//double const ONE_SECOND_IN_NANOSECONDS = 1000000000;
double const NANOSECONDS_IN_ONE_SECOND = 1000000000;
int counter = 0;

//Declares two variables timeStart and timeEnd of type struct timespec. 
//These variables are used to store the start and end times for performance measurement.
struct timespec timeStart, timeEnd;

//Declares an enumeration debugLevelEnum with two possible values LOG_STANDARD and LOG_DEBUG. 
//It also declares a variable debugLevel of this enumeration type and initializes it to LOG_STANDARD.
enum debugLevelEnum {
    LOG_STANDARD, LOG_DEBUG
} debugLevel = LOG_STANDARD;
//----------

/*
 * Function to fill clock at beggining of counting
 */
void get_init_time() {
    if (clock_gettime(CLOCK_REALTIME, &timeStart)) {
        cerr << "clock gettime";
        exit(EXIT_FAILURE);
    }
}

/*
 * Function to fill clock at end of counting
 */
void get_end_time() {
    if (clock_gettime(CLOCK_REALTIME, &timeEnd)) {
        cerr << "clock gettime";
        exit(EXIT_FAILURE);
    }
}

/*
 * Function to calculate difference between two timespec (tv_sec, tv_nsec) at End - Start
 */
struct timespec my_diff() {
    struct timespec temp;
    // If the difference in nanoseconds is negative, then one second should be added
    // to the nanoseconds field and borrow one second from the seconds field.
    // It's like a subtraction where you borrow '1' from the next most significant number.
    if ((timeEnd.tv_nsec - timeStart.tv_nsec) < 0) {
        temp.tv_sec = timeEnd.tv_sec - timeStart.tv_sec - 1;
        temp.tv_nsec = 1000000000 + timeEnd.tv_nsec - timeStart.tv_nsec;
    } else {
        temp.tv_sec = timeEnd.tv_sec - timeStart.tv_sec;
        temp.tv_nsec = timeEnd.tv_nsec - timeStart.tv_nsec;
    }
    return temp;
}

/*
 * Print status when closing program
 */
void print_stats(long double optValue, int ni) {

    if (debugLevel == LOG_DEBUG || debugLevel == LOG_STANDARD) {
        //calculating time difference
        struct timespec time = my_diff();
        //summing time in seconds with time in nanoseconds
        double processTime = (time.tv_sec + (double) time.tv_nsec / NANOSECONDS_IN_ONE_SECOND);

        cout << "time = " << processTime / ni << "| timeTotal = " << processTime << endl;

        cout << "iterations : " << ni << " optimization value : " << optValue << endl;
    }
}

/*
 * Print array
 */
template <class T>
void print_array(T * array, int length) {
    if (debugLevel == LOG_DEBUG) {
        for (int i = 0; i < length; i++) {
            cout << array[i] << " ";
        }
        cout << endl;
    }

}

/*
 * Print matrix
 */
void display_matrix(long double * * mat, int rSz, int cSz) {
    if (debugLevel == LOG_DEBUG) {
        for (int r = 0; r < rSz; r++) {
            for (int c = 0; c < cSz; c++)
                cout << mat[ r ][ c ] << "\t";
            cout << "\n";
        }
    }
}

void display_matrix(long double * * mat, int rSz, int cSz, int k, int dim) {
    if (debugLevel == LOG_DEBUG) {

        if (dim == 1) {
            for (int c = 0; c < cSz; c++)
                cout << mat[ k ][ c ] << "\t";
            cout << "\n";
        }

        if (dim == 2) {
            for (int l = 0; l < rSz; l++)
                cout << mat[ l ][ k ] << "\t";
            cout << "\n";
        }
    }
}

/*
 * Function to configure logger value
 */
void set_debug_level(char* argv) {
    if (argv != NULL && !strcmp(argv, "LOG_DEBUG"))debugLevel = LOG_DEBUG;
}

/*
 *Debug 1
 */
void debug(string msg) {
    if (debugLevel == LOG_DEBUG) {
        cout << msg << endl;
    }
}

void debug_track(string msg) {
    if (debugLevel == LOG_DEBUG) {
        cout << msg << " : " << counter << endl;
        counter++;
    }
}

/*
 *Debug 2
 */
template <class T>
void debug(string msg, T v) {
    if (debugLevel == LOG_DEBUG) {
        cout << msg << " : " << v << endl;
    }
}

/*
 *Debug 3
 */
void debug(int rest, int var) {
    if (debugLevel == LOG_DEBUG) {
        cout << "DebugLevel: " << debugLevel << endl;
        cout << "Problem size: " << rest << "x" << var << endl;
    }
}

/*
 *Debug 4
 */
void debug(long double ** matrix_constraints, int * index_constraint_va,
        int * index_variable_va, int *dimension, int index_va_size) {

    if (debugLevel == LOG_DEBUG) {
        cout << "Array of Index of Constrants of VA:\n";
        print_array(index_constraint_va, index_va_size);
        cout << "Array of Index of Variables of VA:\n";
        print_array(index_variable_va, index_va_size);
        cout << "Dimension of Matrix:\n";
        print_array(dimension, 2);
        cout << "Matrix of Constraints:\n";
        display_matrix(matrix_constraints, dimension[0], dimension[1]);

    }
}


#endif /* DEBUG_H */

