// ----------------------------------------------------------------------------
/**
 * @file  mainS.cpp
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    01/2018
 * 
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 *
 * @brief This file contains a sequential implementation of the standard
 *  simplex algorithm for solving linear programming problems using OpenMP.
 * @language: C++
 *
 * @section Description
 *  The current implementation uses the standard simplex algorithm in tabular
 *  form. The LP(Linear Programming) problem needs to be modeled
 *  in the standard form and can not have artificial variables.
 *
 *  Standard form:
 *  maximize z = c^t * x,
 *  Subject to Ax = b,
 *             x >= 0
 *
 *
 * @subsection Input Data
 *
 *  The input data is a file composed of the constraint matrix 'A', the vector of
 *  coefficients of the objective function 'c' and the vector of independent values 'b',
 *  as shown below. So, the independent values are in the last column and the objectives
 *  values are in the last row.
 *
 *  | A  b|
 *  |-c  0|
 *
 * @subsection Compilation
 *   To compile this file you need to run the following command:
 *
 *  g++ -I./cpp/include -Ofast simplexParallel.cpp -fopenmp -Wall -msse2 -fopt-info-vec-optimized  -ftree-vectorizer-verbose=2  -Wvector-operation-performance -o exec_name
 *
 * @subsection usage
 *   To run execute the command:
 *
 *   ./exec_name /PATH/name_of_input_file numb_of_threads chunk
 *
 *   chunk is a positive integer that specifies a chunk size of a chunk-sized block of loop
 *   iterations to give to each thread.
 *   The file's name needs to be like: number_of_constraintxnumber_of_variables.
 *   Example of a usage with a 2000x2000 LP problem, 16 threads and a chunk of 1000:
 *
 *   ./exec_name 2000x2000 16 1000
 * 
 * @subsection development history
 * Proposed by Demetrios and used in the first version of paper - tested and verified
 * Modified by Victor - under test and verification
 * 
 */
// ----------------------------------------------------------------------------

#include <chrono>
#include <ctime>
#include "time.h"
#include "functions.h"

using namespace std;

double** tableau;

int main(int argc, char** argv) {

    int nRow, nCol, numbThreads, chunk, ni = 0;
    int i, j, k, pivot_col, pivot_row, negatives, count;
    double maxN = 0, minN, pivot, temp;

    struct timespec timeTotalInit, timeTotalEnd;

    tableau = read_data(argv[1], nRow, nCol);

    // Update log file
    // Declare a variable named log_file of the type ofstream (Output File Stream).
    ofstream log_file;

    log_file.open("./logs/log_seq",ofstream::app);

    if(!log_file.is_open()) {
    cerr << "Error: Could not open log file." << endl;
    return 1; // or another appropriate error code
    }

    log_file << "sequential simplex" << endl;
    log_file << "filename: " << argv[1] << endl;
    log_file << "----" << nRow << "x" << nCol <<"----"<< endl;
    log_file << "nRow: " << nRow << endl;
    log_file << "nCol: " << nCol << endl;

    nRow--;
    nCol--;

    from_string<int>(numbThreads, string(argv[2]), std::dec);
    log_file << "numbThreads: " << numbThreads << endl;

    from_string<int>(chunk, string(argv[3]), std::dec);
    log_file << "chunk: " << chunk << endl;

    // Get the current time as a timespec
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts)) {
    perror("Error in retrieving system time");
    exit(EXIT_FAILURE);
    }

    // Convert the timespec to a time_point
    chrono::time_point<chrono::system_clock> tp =
    chrono::system_clock::from_time_t(ts.tv_sec) +
    chrono::nanoseconds(ts.tv_nsec);

    // Get the date and time components from the time_point
    time_t tt = chrono::system_clock::to_time_t(tp);
    struct tm tm;
    localtime_r(&tt, &tm); // Use localtime_r on POSIX systems
    int year = tm.tm_year + 1900;
    int month = tm.tm_mon + 1;
    int day = tm.tm_mday;
    int hour = tm.tm_hour;
    int minute = tm.tm_min;
    int second = tm.tm_sec;

    // Print the date and time in a formatted way
    log_file << "The current date and time is: " << year << "-" << month << "-"
                << day << " " << hour << ":" << minute << ":" << second << "."
                << "\n";

    ni = 0;

    if (clock_gettime(CLOCK_REALTIME, &timeTotalInit)) {
        perror("Error in retrieving system time");
        exit(EXIT_FAILURE);
    }

    do {

        maxN = 0;
        pivot_col = -1;

        negatives = 0;
        minN = HUGE_VALL;
        pivot_row = -1;

        for (j = 0; j < nCol; j++) {
            if (tableau[nRow][j] < 0) {

                temp = -1 * tableau[nRow][j];
                if (maxN < temp) {
                    maxN = temp;
                    pivot_col = j;

                }
            } else
                continue;
        }


        for (i = 0; i < nRow; i++) {
            if (tableau[i][pivot_col] > 0) {
                pivot = tableau[i][nCol] / tableau[i][pivot_col];

                if (minN > pivot) {
                    minN = pivot;
                    pivot_row = i;
                }
            } else {
                negatives++;
            }
        }

        if (negatives == nRow) {
            printf("Solution not found\n");
            exit(1);
        }

        pivot = tableau[pivot_row][pivot_col];

        for (j = 0; j <= nCol; j++)
            tableau[pivot_row][j] = tableau[pivot_row][j] / pivot;

        for (i = 0; i <= nRow; i++) {
            if (i != pivot_row) {
                pivot = tableau[i][pivot_col] * -1;

                for (k = 0; k <= nCol; k++) {
                    tableau[i][k] += (pivot * tableau[pivot_row][k]);
                }
            }
        }

        count = 0;
        for (j = 0; j < nCol; j++)
            if (tableau[nRow][j] >= 0)
                count++;

        ni++;

        if (count == nCol)
            break;
    } while (count);

    if (clock_gettime(CLOCK_REALTIME, &timeTotalEnd)) {
        perror("clock gettime");
        exit(EXIT_FAILURE);
    }

    double NANOSECONDS_IN_ONE_SECOND = 1000000000;
    struct timespec time = My_diff(timeTotalInit, timeTotalEnd);
    double processTime = (time.tv_sec + (double) time.tv_nsec / NANOSECONDS_IN_ONE_SECOND);


    // printf("%s %s %s %s %s %s %s %s %s %s %s \n", "filename ","nRow ","nCol ","numthreads ",
    // "chunk ","ni ","processtime/ni ","processtime ", "obj_value", "run", "method");
    printf("%s %d % d %s %s %d %.9f %.9f %f %s %s \n", argv[1], nRow +1, nCol +1, argv[2], argv[3], ni,
    processTime / ni, processTime, tableau[nRow][nCol], argv[4], "sequential");

    // printf("%f %f ", processTime / ni, processTime);
    // printf("%d %f \n", ni, tableau[nRow][nCol]);

    // printMatrix(tableau, nRow, nCol);

    log_file << "processTime / ni: " << processTime / ni << endl;
    log_file << "processTime: " << processTime  << endl;
    log_file << "no of iterations (ni): " << ni  << endl;
    log_file << "Optimal value: " << tableau[nRow][nCol]  << endl;

    log_file.close();

    if(log_file.fail()) {
    cerr << "Error: Could not close log file." << endl;
    return 1; // or another appropriate error code
    }

    delete_matrix(tableau, nRow + 1);
}
