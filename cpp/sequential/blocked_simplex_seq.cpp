// ----------------------------------------------------------------------------
/**
 * @file  mainS.cpp
 * @author Demétrios A. M. Coutinho
 * @email demetriosamc@gmail.com
 * @author Samuel Xavier de Souza
 * @email samuel@dca.ufrn.br
 * @date    01/2018
 * 
 * @author Victor Rafael R. Celestino
 * @email vrcelestino@unb.br
 * @date    10/2023
 *
 * @brief This file contains a sequential implementation of the standard
 *  simplex algorithm for solving linear programming problems using OpenMP.
 * @language: C++
 *
 * @section Description
 *  The current implementation uses the standard simplex algorithm in tabular
 *  form. The LP(Linear Programming) problem needs to be modeled
 *  in the standard form and can not have artificial variables.
 *
 *  Standard form:
 *  maximize z = c^t * x,
 *  Subject to Ax = b,
 *             x >= 0
 *
 *
 * @subsection Input Data
 *
 *  The input data is a file composed of the constraint matrix 'A', the vector of
 *  coefficients of the objective fucolSizetion 'c' and the vector of independent values 'b',
 *  as shown below. So, the independent values are in the last column and the objectives
 *  values are in the last row.
 *
 *  | A  b|
 *  |-c  0|
 *
 * @subsection Compilation
 *   To compile this file you need to run the following command:
 *
 *  g++ -I./cpp/include -Ofast simplexParallel.cpp -fopenmp -Wall -msse2 -fopt-info-vec-optimized  -ftree-vectorizer-verbose=2  -Wvector-operation-performacolSizee -o exec_name
 *
 * @subsection usage
 *   To run execute the command:
 *
 *   ./exec_name /PATH/name_of_input_file blockingsizeB(instead number of threads) chunk
 *
 *   chunk is a positive integer that specifies a chunk size of a chunk-sized block of loop
 *   iterations to give to each thread.
 *   The file's name needs to be like: number_of_constraintxnumber_of_variables.
 *   Example of a usage with a 2000x2000 LP problem, 16 threads and a chunk of 1000:
 *
 *   ./exec_name 2000x2000 16 1000
 *  *
 * @subsection development history
 * Proposed by Felipe using blocked simplex - still in development
 *
 */
// ----------------------------------------------------------------------------

#include "newfunctions.h"
#include <iostream>
#include <cmath>


using namespace std;

double* tableau;

// void printMatrix(double* v, int nRow, int nCol)
// {
//   for (int i = 0 ; i < nRow; i++)
//   {
//     for (int j = 0; j < nCol; j++)
//     {
//       cout << v[i * nCol + j] << " ";
//     }
//     cout << endl;
//   }
// }

int main(int argc, char** argv) {

    int nRow, nCol, ni;
    int pivot_col, pivot_row, negatives, count;
    double maxN = 0, minN = 1, pivot, temp;

    struct timespec timeTotalInit, timeTotalEnd;

    tableau = read_data(argv[1], nRow, nCol);

    nRow--;
    nCol--;
    ni = 0;

    // The blocking size is passed when executing as the second paramenter 
    int B;
    B = atoi(argv[2]);

    // Suggested code for validation of the blocking size
    /*
    if (rowSize % B != 0 || colSize % B != 0) {
    cout << "Error: Block size must divide evenly into tableau dimensions." << endl;
    exit(EXIT_FAILURE);
    }
    */


    if (clock_gettime(CLOCK_REALTIME, &timeTotalInit)) {
        perror("Error in retrieving system time");
        exit(EXIT_FAILURE);
    }


    do {

      maxN = 0;
      pivot_col = -1;
      count = 0;

      minN = HUGE_VAL;
      pivot_row = -1;
      negatives = 0;

      ni++;

      // SELECT THE COLUMN INDEX WITH THE MINIMUM
      // NEGATIVE VALUE OF THE OBJECTIVE ROW
      //  CONSIDER PARALLELIZATION FOR REDUCTION +

      for(int j = 0; j < nCol; j++)
      {
        if(tableau[nRow * (nCol + 1) + j] < 0) {
          temp = -tableau[nRow * (nCol + 1) + j];
          if (temp > maxN) {
              maxN = temp;
              pivot_col = j;
          }
        }
        else
          count++;
      }

      if ( count == nCol )
        break;


      // SELECT THE ROW INDEX WITH THE
      // MINIMUM RATIO
      // CONSIDER PARALLELIZATION FOR REDUCTION NO NEGATIVE +

      for (int i = 0; i < nRow; i++)
      {
        if(tableau[i * (nCol + 1) + pivot_col] > 0) {
          pivot = tableau[i * (nCol + 1) + nCol]/ tableau[i * (nCol + 1) + pivot_col];
          if( pivot < minN) {
            minN = pivot;
            pivot_row = i;
          }
        } else
            negatives++;
      }
      
      // CONSIDER BARRIER 1
      // Verify if here should not be Barrier 2
      if (negatives == nRow)
      {
        cout << "Solution not found\n";
        exit(EXIT_FAILURE);
      }

      // BARRIER 2
      // GAUSSIAN REDUCTION USING BLOCKING

      pivot = 1/tableau[pivot_row * (nCol + 1) + pivot_col];

      for (int j = 0; j < nCol; j++) {
          tableau[pivot_row * (nCol + 1) + j] *= pivot;
        }


      // block_init (previous code)
      for (int block_init = pivot_col + 1; block_init < nCol + 1; block_init += B) {
        int lim = nCol + 1  < block_init + B ? nCol + 1 : B + block_init;

        // NORMALIZING PIVOT ROW
        // CONSIDER PARALLELIZATION FOR REDUCTION *
        // for (int j = block_init; j < lim; j++) // vectorized
        // {
        //   tableau[pivot_row * (nCol + 1) + j] *= pivot;
        // }

        // IMPLICIT BARRIER
        ///////////////////////////////////////////////////////////////////////////////////
        // FOR PARALLELIZED WITH NOWAIT

        for (int current_row = 0; current_row < pivot_row; current_row++)
        {
          temp = tableau[current_row * (nCol + 1) + pivot_col];
          for (int inner_block = block_init; inner_block < lim; inner_block++) // vectorized
          {
            tableau [
                      ( current_row * (nCol + 1) + (inner_block))
                    ]
                    -=
                          tableau [ pivot_row * (nCol + 1) + (inner_block)] * temp;
          }
        }

        /////////////////////////////////////////////////////////////////////////////////////
        
        // FOR PARALLELIZED ( IMPLICIT BARRIER)

        for (int current_row = pivot_row + 1; current_row < nRow + 1; current_row++)
        {
          temp = tableau[current_row * (nCol + 1) + pivot_col];
          for (int inner_block = block_init; inner_block < lim; inner_block++) // vetorizado
          {
            tableau [
                      ( current_row * (nCol + 1) + (inner_block))
                    ]
                    -=
                          tableau [ pivot_row * (nCol + 1) + (inner_block)] * temp;
          }
        }

        // IMPLICIT BARRIER
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      }

      for (int block_init = 0; block_init <= pivot_col; block_init += B) {
        int lim = pivot_col + 1 < block_init + B ? pivot_col + 1 : B + block_init;
        //int lim = std::min(pivot_col + 1, block_init + B);

        // NORMALIZING PIVOT ROW
        // FOR PARALLELIZED WITH REDUCTION *
        for (int j = block_init; j < lim; j++) { // vectorized
        
          tableau[pivot_row * (nCol + 1) + j] *= pivot;
        }

        // BARRIER
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // FOR PARALLELIZED WITH NOWAIT

        for (int current_row = 0; current_row < pivot_row; current_row++)
        {
          temp = tableau[current_row * (nCol + 1) + pivot_col];
          for (int inner_block = block_init; inner_block < lim; inner_block++) // vectorized
          {
            tableau [
                      ( current_row * (nCol + 1) + (inner_block))
                    ]
                    -=
                          tableau [ pivot_row * (nCol + 1) + (inner_block)] * temp;
          }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // FOR PARALLELIZED

        for (int current_row = pivot_row + 1; current_row < nRow + 1; current_row++) {
          temp = tableau[current_row * (nCol +1) + pivot_col];
          for (int inner_block = block_init; inner_block < lim; inner_block++) // vectorized
          {
            tableau [
                      ( current_row * (nCol + 1) + (inner_block))
                    ]
                    -=
                          tableau [ pivot_row * (nCol + 1) + (inner_block)] * temp;
          }
        }

        // IMPLICIT BARRIER
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      }


    } while (count);

    if (clock_gettime(CLOCK_REALTIME, &timeTotalEnd)) {
        perror("Error in retrieving system time");
        exit(EXIT_FAILURE);
    }

    double NANOSECONDS_IN_ONE_SECOND = 1000000000;
    struct timespec time = My_diff(timeTotalInit, timeTotalEnd);
    double processTime = (time.tv_sec + (double) time.tv_nsec / NANOSECONDS_IN_ONE_SECOND);

    printf("%f %f ", processTime / ni, processTime);

    printf("%d %f \n", ni, tableau[nRow * (nCol + 1) * nCol]);

    log_file.close();

  printMatrix(tableau, nRow + 1, nCol + 1);
  delete_matrix(tableau, nRow + 1);
}
